const cp = require('child_process');
const fs = require('fs');
const path = require('path');

const packageJson = require('./package.json');
const packageVersion = packageJson.version;

const command = "git rev-list --format=format:'%ai' --max-count=1 `git rev-parse HEAD`";

const outputFile = path.join(__dirname, 'VERSION');

cp.exec(command, (err, stdout, stderr) => {
    if (err || stderr) {
        console.error(err || stderr);
        return;
    }

    const [commitInfo] = stdout.split('\n');
    const commit = commitInfo.split(' ')[1].substring(0, 7);

    const versionString = `${packageVersion}-${commit}`;
    fs.writeFileSync(outputFile, versionString, 'utf8');
});
