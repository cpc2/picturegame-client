import { arrayShallowEqual } from './Equality';

export function createMemoizedArray<P extends any[], T>(getArray: (...args: P) => T[]) {
    let array: T[];
    return (...args: P) => {
        const nextArray = getArray(...args);
        if (!arrayShallowEqual(nextArray, array)) {
            array = nextArray;
        }
        return array;
    };
}
