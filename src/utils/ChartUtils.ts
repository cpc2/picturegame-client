import * as React from 'react';

import { useEvent } from '../hooks/Events';
import { useReplicatedState } from '../hooks/Misc';

import {
    addDays, addMonths, addYears,
    moveToStartOfDay, moveToStartOfMonth, moveToStartOfWeek, moveToStartOfYear,
} from './DateUtils';

export enum FieldValueType {
    Duration = 'duration',
    TimeOfDay = 'timeOfDay',
    DayOfWeek = 'dayOfWeek',
    Date = 'date',
    Username = 'username',
    NumRounds = 'numRounds',
    NumPlayers = 'numPlayers',
}

export type DataType = 'string' | 'number' | 'duration' | 'timeOfDay' | 'dayOfWeek' | 'date' | 'month' | 'monthYear' | 'year';

export const ValueTypeNames: Readonly<Record<FieldValueType, string>> = {
    [FieldValueType.Duration]: 'Duration',
    [FieldValueType.TimeOfDay]: 'Time of Day',
    [FieldValueType.DayOfWeek]: 'Day of Week',
    [FieldValueType.Date]: 'Date',
    [FieldValueType.Username]: 'Player',
    [FieldValueType.NumPlayers]: 'Number of Players',
    [FieldValueType.NumRounds]: 'Number of Rounds',
};

const MsPerDay = 1000 * 60 * 60 * 24;
const YearThreshold = MsPerDay * 365 * 6;
const HalfYearThreshold = MsPerDay * 365 * 3;
const QuarterYearThreshold = MsPerDay * 365 * 1.5;
const MonthThreshold = MsPerDay * 180;
const WeekThreshold = MsPerDay * 56;
const DayThreshold = MsPerDay * 14;

export function calculateLinearDateTicks(xMin: number, xMax: number) {
    const startDate = new Date(xMin * 1000);
    const endDate = new Date(xMax * 1000);
    const duration = endDate.getTime() - startDate.getTime();

    const ticks: number[] = [];
    const currentDate = new Date(startDate.getTime());

    if (duration < DayThreshold) {
        moveToStartOfDay(currentDate);
        while (addDays(currentDate, 1) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }

    } else if (duration < WeekThreshold) {
        moveToStartOfWeek(currentDate);
        while (addDays(currentDate, 7) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }

    } else if (duration < MonthThreshold) {
        moveToStartOfMonth(currentDate);
        while (addMonths(currentDate, 1) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }

    } else if (duration < QuarterYearThreshold) {
        moveToStartOfMonth(currentDate);
        currentDate.setUTCMonth(currentDate.getUTCMonth() - (currentDate.getUTCMonth() % 3));
        while (addMonths(currentDate, 3) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }

    } else if (duration < HalfYearThreshold) {
        moveToStartOfMonth(currentDate);
        currentDate.setUTCMonth(currentDate.getUTCMonth() - (currentDate.getUTCMonth() % 6));
        while (addMonths(currentDate, 6) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }

    } else if (duration < YearThreshold) {
        moveToStartOfYear(currentDate);
        while (addYears(currentDate, 1) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }

    } else {
        moveToStartOfYear(currentDate);
        while (addYears(currentDate, 2) <= endDate) {
            ticks.push(Math.floor(currentDate.getTime() / 1000));
        }
    }

    return ticks;
}

export function calculateLinearTicks(minVal: number, maxVal: number, dataType?: DataType, baseInterval?: number) {
    const range = maxVal - minVal;
    const interval = calculateInterval(range, dataType, baseInterval);
    let i = minVal - (minVal % interval);
    const results: number[] = [];
    do {
        results.push(i);
        i += interval;
    } while (i <= maxVal);

    if (results[results.length - 1] < maxVal) {
        results.push(i);
    }

    return results;
}

function calculateInterval(range: number, dataType: DataType | undefined, baseInterval: number = 1) {
    if (dataType === 'duration' && durationIntervals.includes(baseInterval)) {
        return calculateDurationInterval(range, baseInterval);
    }

    if (dataType === 'timeOfDay' && timeOfDayIntervals.includes(baseInterval)) {
        return calculateTimeOfDayInterval(range, baseInterval);
    }

    return calculateNumberInterval(range, baseInterval);
}

const intervals = [1, 2, 5];
function calculateNumberInterval(range: number, baseInterval: number = 1) {
    let mag = 0;
    while (true) {
        const base = baseInterval * Math.pow(10, mag++);
        for (const mult of intervals) {
            const interval = base * mult;
            const numTicks = Math.ceil(range / interval);
            if (numTicks <= 8) {
                return interval;
            }
        }
    }
}

const durationIntervals = [1, 5, 10, 15, 20, 30, 60, 120, 300, 600, 900, 1800, 3600];
function calculateDurationInterval(rangeSec: number, baseInterval: number = 1) {
    for (const interval of durationIntervals) {
        if (interval < baseInterval || interval % baseInterval) {
            continue;
        }

        const numTicks = Math.ceil(rangeSec / interval);
        if (numTicks <= 8) {
            return interval;
        }
    }

    const base = durationIntervals[durationIntervals.length - 1];
    let mag = 0;
    while (true) {
        const baseMag = base * Math.pow(10, mag++);
        for (const mult of intervals) {
            const interval = baseMag * mult;
            const numTicks = Math.ceil(rangeSec / interval);
            if (numTicks <= 8) {
                return interval;
            }
        }
    }
}

const timeOfDayIntervals = [60, 300, 600, 900, 1200, 1800, 3600, 7200, 10800, 14400, 21600];
function calculateTimeOfDayInterval(rangeSec: number, baseInterval: number = 1) {
    for (const interval of timeOfDayIntervals) {
        if (interval < baseInterval || interval % baseInterval) {
            continue;
        }

        const numTicks = Math.ceil(rangeSec / interval);
        if (numTicks <= 8) {
            return interval;
        }
    }

    return 86400;
}

export function getMaxValue<T>(data: T[], getValues: (value: T) => number[]): number {
    let dataMax: number = 0;
    for (const dataPoint of data) {
        for (const value of getValues(dataPoint)) {
            if (value > dataMax) {
                dataMax = value;
            }
        }
    }

    return dataMax;
}

export function getYRange<T extends { [key: string]: number | string }>(
    data: T[], xMin: number, xMax: number, xKey: keyof T, getYValues: (value: T) => number[]): [number, number] {

    let dataMin: number = Number.MAX_SAFE_INTEGER;
    let dataMax: number = 0;
    for (const dataPoint of data) {
        if (dataPoint[xKey] >= xMin && dataPoint[xKey] <= xMax) {
            for (const value of getYValues(dataPoint)) {
                if (value < dataMin) {
                    dataMin = value;
                }
                if (value > dataMax) {
                    dataMax = value;
                }
            }
        }
    }

    if (dataMin === Number.MAX_SAFE_INTEGER) {
        dataMin = 0;
    }

    return [dataMin, dataMax];
}

export type ZoomDomain = [number, number] | null;

export interface ChartZoomProps<T> {
    data?: T[];
    xKey: keyof T;
    activeDomain: ZoomDomain;
    setActiveDomain(domain: ZoomDomain): void;
    dragToZoomActive: boolean;
}

export interface ChartZoomHooks {
    xMin: number;
    xMax: number;
    dragStart: number | null;
    dragEnd: number | null;
    onMouseDown: (e: any) => void;
    onMouseMove: (e: any) => void;
    containerRef: React.RefObject<HTMLDivElement>;
}

export function useChartZoom<T extends { [key: string]: number | string }>(props: ChartZoomProps<T>): ChartZoomHooks {
    const { data, xKey, activeDomain, setActiveDomain } = props;

    const containerRef = React.useRef<HTMLDivElement>(null);
    const containerDimsRef = React.useRef<[number, number]>([NaN, NaN]);
    function updateContainerDims() {
        const xAxisLine = containerRef.current?.querySelector('.recharts-xAxis > .recharts-cartesian-axis-line');
        const leftX = parseInt(xAxisLine?.getAttribute('x')!, 10);
        const width = parseInt(xAxisLine?.getAttribute('width')!, 10);
        containerDimsRef.current = [leftX, width];
    }

    const [xMin, xMax] = activeDomain ?? [data?.[0]?.[xKey] as number ?? 0, data?.[data.length - 1]?.[xKey] as number ?? 1];
    React.useEffect(() => { setActiveDomain(null); }, [data?.[0]?.[xKey]]);

    const { value: dragStart, setValue: setDragStart, refValue: dragStartRef } = useReplicatedState<number | null>(null);
    const { value: dragEnd, setValue: setDragEnd, refValue: dragEndRef } = useReplicatedState<number | null>(null);

    function onZoom() {
        const currentStart = dragStartRef.current;
        const currentEnd = dragEndRef.current;

        setDragStart(null);
        setDragEnd(null);

        if (currentStart === null || currentEnd === null || currentStart === currentEnd) {
            return;
        }

        const domain: ZoomDomain = currentEnd > currentStart
            ? [currentStart, currentEnd] : [currentEnd, currentStart];

        setActiveDomain(domain);
    }

    function calculateGraphX(chartX: number) {
        const [containerLeftX, containerWidth] = containerDimsRef.current;
        if (typeof chartX === 'number' && typeof containerLeftX === 'number' && typeof containerWidth === 'number') {
            return (chartX - containerLeftX) / containerWidth * (xMax - xMin) + xMin;
        }
        return null;
    }

    function onMouseDown(e: any) {
        if (props.dragToZoomActive) {
            updateContainerDims();
            setDragStart(calculateGraphX(e?.chartX) ?? null);
        }
    }

    function onMouseMove(e: any) {
        if (typeof dragStart !== 'number' || typeof xMin !== 'number' || typeof xMax !== 'number') {
            return;
        }

        const graphX = calculateGraphX(e?.chartX);
        if (typeof graphX === 'number') {
            setDragEnd(graphX);
        } else {
            // Dragged off the left or right edge of the chart
            // make an educated guess which side based on the last known position
            const distLeft = (dragEnd ?? dragStart) - xMin;
            const distRight = xMax - (dragEnd ?? dragStart);
            setDragEnd(distLeft < distRight ? xMin : xMax);
        }
    }

    useEvent('pointerup', onZoom);

    return {
        xMin,
        xMax,
        dragStart,
        dragEnd,
        onMouseDown,
        onMouseMove,
        containerRef,
    };
}

export interface ZoomableChartProps {
    zoomToDomain: ZoomDomain;
    onZoom(domain: ZoomDomain): void;
    dragToZoomActive: boolean;
    onToggleDragToZoom: () => void;
}

export function useZoomState(): ZoomableChartProps {
    const [zoomToDomain, setZoomToDomain] = React.useState<ZoomDomain>(null);
    const [dragToZoomActive, setDragToZoomActive] = React.useState<boolean>(false);
    const onToggleDragToZoom = React.useCallback(() => setDragToZoomActive(v => !v), [setDragToZoomActive]);
    const onZoom = React.useCallback((domain: [number, number] | null) => {
        setZoomToDomain(domain);
        setDragToZoomActive(false);
    }, [setZoomToDomain, setDragToZoomActive]);

    return { zoomToDomain, onZoom, dragToZoomActive, onToggleDragToZoom };
}
