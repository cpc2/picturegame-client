export function roundToPowerTen(num: number, roundFn?: (v: number) => number): number {
    if (!isFinite(num)) {
        return num;
    }

    let mag = 1;
    while (num > 10) {
        mag *= 10;
        num /= 10;
    }
    return (roundFn ?? Math.round)(num) * mag;
}
