import * as _ from 'lodash-es';

export function shallowEqual<T>(a: T, b: T): boolean {
    if (a === b) {
        return true;
    }

    if (!_.isObject(a) || !_.isObject(b)) {
        return a === b;
    }

    const aKeys = Object.keys(a) as (keyof T)[];
    const bKeys = Object.keys(b) as (keyof T)[];

    if (aKeys.length !== bKeys.length) {
        return false;
    }

    for (const key of aKeys) {
        if (a[key] !== b[key]) {
            return false;
        }
    }

    return true;
}

export function arrayShallowEqual<T extends any[]>(a: T, b: T): boolean {
    if (a === b) {
        return true;
    }

    if (!_.isObject(a) || !_.isObject(b)) {
        return a === b;
    }

    if (a.length !== b.length) {
        return false;
    }

    for (const i in a) {
        if (!shallowEqual(a[i], b[i])) {
            return false;
        }
    }

    return true;
}
