import * as Immutable from 'immutable';
import * as API from 'picturegame-api-wrapper';
import * as Reselect from 'reselect';

import * as UI from '../model/ui';

import { formatDate } from './Formatter';
import { createMemoizedArray } from './Memoize';

export interface RoundWinner {
    winTime: number;
    winner: string;
}
export const getSortedRounds: (lb: Immutable.Map<string, API.LeaderboardEntry>) => RoundWinner[]
    = Reselect.createSelector(
        (lb: Immutable.Map<string, API.LeaderboardEntry>) => lb,
        function _getSortedRounds(lb): RoundWinner[] {
            const rounds: RoundWinner[] = [];
            lb.forEach((player, username) => {
                player.winTimeList?.forEach(winTime => {
                    rounds.push({ winTime, winner: username });
                });
            });
            return rounds.sort((a, b) => a.winTime - b.winTime);
        });

export interface RankChange {
    time: number;
    from: number | null;
    to: number;
    end?: number;
}
interface PlayerData {
    username: string;
    numWins: number;
}
export const getRankChanges: (state: UI.State) => RankChange[] = Reselect.createSelector(
    (state: UI.State) => getSortedRounds(state.data.leaderboard),
    (state: UI.State) => state.data.leaderboard,
    function _getRankChanges(rounds, leaderboard): RankChange[] {
        const data: RankChange[] = [];
        const players = new Map<string, number>();
        const lb: PlayerData[] = [];

        function changeRank(time: number, from: number | null, to: number, end?: number) {
            if (time) {
                data.push({ from, to, time, end });
            }
        }

        for (const { winner, winTime } of rounds) {
            // 1-indexed - lb[currentRank - 1] is the current winner, lb[currentRank - 2] is the next rank etc
            const startRank = players.get(winner);
            if (!startRank) {
                lb.push({ numWins: 1, username: winner });
                const rank = lb.length;
                players.set(winner, rank);
                changeRank(winTime, null, rank, leaderboard.get(winner)?.rank);
                continue;
            }

            let currentRank = startRank;
            const player = lb[currentRank - 1];
            player.numWins++;

            let user: PlayerData;
            while (currentRank > 1 && (user = lb[currentRank - 2]).numWins < player.numWins) {
                players.set(user.username, currentRank);
                lb[currentRank - 1] = user;
                currentRank--;
            }

            if (startRank === currentRank) {
                continue;
            }

            lb[currentRank - 1] = player;
            players.set(winner, currentRank);
            changeRank(winTime, startRank, currentRank);
        }

        return data;
    });

export const getPlayerData: (state: UI.State, players: API.LeaderboardEntry[]) => UI.Player[] = Reselect.createSelector(
    (state: UI.State) => state.data.playerMetrics,
    createMemoizedArray((_state: UI.State, players: API.LeaderboardEntry[]) => players),
    (state: UI.State, players: API.LeaderboardEntry[]) => getPeakRanks(state, players),
    function _getPlayerData(playerMetrics, players, peakRanks): UI.Player[] {
        return players.map((p): UI.Player => {
            const username = p.username.toLowerCase();
            return {
                player: p,
                metrics: playerMetrics.get(username),
                rankPeaks: peakRanks.get(p.username) ?? [],
            };
        });
    });

export const getPeakRanks: (state: UI.State, players: API.LeaderboardEntry[]) => Map<string, UI.RankIntervalInfo[]>
    = Reselect.createSelector(
        getRankChanges,
        createMemoizedArray((_: UI.State, players: API.LeaderboardEntry[]) => players),
        function _getPeakRanks(rankChanges, activePlayers): Map<string, UI.RankIntervalInfo[]> {
            const result = new Map<string, UI.RankIntervalInfo[]>();

            for (const player of activePlayers) {
                let lastRank: number | null = null;
                let bestRank: number | null = null;

                const peakIntervals: UI.RankIntervalInfo[] = [];
                let currentItem: UI.RankIntervalInfo | null = null;

                for (const { from, to, time, end } of rankChanges) {
                    if (end === player.rank || (from && lastRank && from === lastRank)) {
                        // Active user joined the sub or ranked up
                        // Their previous state must therefore not have been a peak
                        lastRank = to;
                        if (!bestRank || to <= bestRank) {
                            currentItem = { rank: to, fromTime: time };
                            bestRank = to;
                        }
                        continue;
                    }

                    // Active user hasn't joined the sub yet or this is a new player
                    if (!lastRank || !from) { continue; }

                    if (from > lastRank && to <= lastRank) {
                        // Overtook the active player. This could be the end of a peak
                        // The new rank can't be a peak as it's lower rank than before
                        lastRank++;
                        if (currentItem) {
                            currentItem.toTime = time;
                            peakIntervals.push(currentItem);
                            currentItem = null;
                        }
                    }
                }

                if (currentItem) {
                    peakIntervals.push(currentItem);
                }

                result.set(player.username, peakIntervals.filter(i => i.rank === bestRank));
            }

            return result;
        });

export function formatRankPeakDates(peakRanks: UI.RankIntervalInfo[], filterRangeEnd?: Date | null) {
    return peakRanks
        .map(r => `${formatDate(r.fromTime)} - ${formatDate(r.toTime) ?? formatDate(filterRangeEnd) ?? 'present'}`)
        .join('\n');
}
