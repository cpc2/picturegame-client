import * as React from 'react';

export interface ClassStyleProps {
    className?: string;
    style?: React.CSSProperties;
}

interface ClassNameObj {
    [key: string]: boolean;
}

export function classNameFromObj(obj: ClassNameObj): string {
    const classNames: string[] = [];
    for (const key in obj) {
        if (key && obj[key]) {
            classNames.push(key);
        }
    }
    return classNames.join(' ');
}

export function combineProps(
    props: ClassStyleProps,
    className: string | ClassNameObj,
    style?: React.CSSProperties): ClassStyleProps {

    className = typeof (className) === 'string' ? className : classNameFromObj(className);
    if (props.className) {
        className += ' ' + props.className;
    }
    return {
        className,
        style: {
            ...props.style,
            ...style,
        },
    };
}

export const HighlightColours = [
    '#22af46',
    '#ff4300',
    '#169ae4',
    '#ff9100',
    '#eb0bfd',
    '#e4d212',
    '#e4121a',
    '#7112e4',
];

export function getColour(index: number) {
    return HighlightColours[index % HighlightColours.length];
}
