export function getInt(rawValue: string | undefined | null, defaultVal: number) {
    if (!rawValue) {
        return defaultVal;
    }

    const numericValue = parseInt(rawValue, 10);
    return isNaN(numericValue) ? defaultVal : numericValue;
}

export function createStringArrayQuery(params: string[]) {
    return '[' + params.map(p => `"${p}"`).join(',') + ']';
}
