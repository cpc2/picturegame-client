export function delay(ms: number) {
    return new Promise<void>(res => {
        setTimeout(res, ms);
    });
}

export function makeCancellable<T>(promise: Promise<T>) {
    let cancelled = false;

    const wrapped = new Promise<T>((res, rej) => {
        promise.then(
            val => cancelled ? rej({ cancelled }) : res(val),
            err => cancelled ? rej({ cancelled }) : rej(err));
    });

    return {
        promise: wrapped,
        cancel: () => {
            cancelled = true;
        },
    };
}
