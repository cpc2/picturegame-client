import { ListResponse, RoundGroup, SingleRequest, SingleRoundsAggregateRequest } from 'picturegame-api-wrapper';
import * as React from 'react';

import { ApiContext } from '../../api/PicturegameApi';
import { RoundAggFieldTranslations, RoundFieldTranslations } from '../../model/Translation';
import { useZoomState } from '../../utils/ChartUtils';
import { getColour } from '../../utils/CssUtils';

import { CustomChart } from '../custom_charts/CustomChart';
import { ChartConfig, ChartData, CustomChartType } from '../custom_charts/CustomChartHelper';

import { BarChartConfig } from '../custom_charts/BarChart';
import { PieChartConfig } from '../custom_charts/PieChart';
import { ScatterPlotConfig } from '../custom_charts/ScatterPlotChart';
import { TableConfig } from '../custom_charts/Table';
import { TimeSeriesConfig } from '../custom_charts/TimeSeriesChart';

import { RoundAggFields, RoundGroupingFields } from '../custom_charts/ChartSpecs';

import { RoundFieldSpec } from '../filter/FilterSpecs';

import { ChartBuilder } from './ChartBuilder';

import './Sandbox.scss';

interface ChartQuery {
    request: SingleRequest;
    chart: ChartConfig;
}

const barChart: BarChartConfig = {
    header: 'Num wins by day of week for Provium',
    chartType: CustomChartType.Bar,
    primaryKey: 'winDayOfWeek',
    series: [
        {
            key: 'numRounds',
            colour: getColour(2),
        },
        {
            key: 'avgSolveTime',
            colour: getColour(4),
            axis: 2,
        },
    ],
    width: 2,
    legendType: 'bottom',
};

const histogram: BarChartConfig = {
    header: 'Round length distribution',
    chartType: CustomChartType.Histogram,
    primaryKey: 'solveTime',
    interval: 300,
    series: [
        {
            key: 'numRounds',
            colour: getColour(2),
        },
    ],
    width: 2,
};

const pieChart1: PieChartConfig = {
    header: 'Hosts whose rounds Provium has solved',
    chartType: CustomChartType.Pie,
    primaryKey: 'hostName',
    secondaryKey: 'numRounds',
    width: 1,
    combineLessThanPercent: 1,
    legendType: 'bottom',
};
const pieChart2: PieChartConfig = {
    header: 'Hours of the day that Provium has solved rounds',
    chartType: CustomChartType.Pie,
    primaryKey: 'winTimeOfDay',
    secondaryKey: 'numRounds',
    width: 1,
    // combineLessThanPercent: 1,
    legendType: 'right',
};
const scatterPlot: ScatterPlotConfig = {
    header: 'Monthly num winners vs num hosts',
    chartType: CustomChartType.Scatter,
    primaryKey: 'winTime',
    xKey: 'numHosts',
    yKey: 'numWinners',
    colour: 'blue',
    width: 2,
    legendType: 'bottom',
};
const timeSeries: TimeSeriesConfig = {
    header: 'Num rounds and min solve time by month',
    chartType: CustomChartType.TimeSeries,
    primaryKey: 'winTime',
    series: [
        {
            display: 'bar',
            key: 'numRounds',
            colour: getColour(3),
        },
        {
            display: 'line',
            key: 'minSolveTime',
            colour: getColour(5),
            axis: 2,
        },
    ],
    width: 3,
    legendType: 'bottom',
};

const table: TableConfig = {
    chartType: CustomChartType.Table,
    header: 'Fastest solvers',
    primaryKey: 'winnerName',
    series: [
        {
            key: 'index',
            width: 40,
        },
        {
            key: 'winnerName',
            width: 200,
        },
        {
            key: 'avgSolveTime',
            width: 100,
        },
        {
            key: 'numRounds',
            width: 100,
        },
    ],
};

const requests: SingleRoundsAggregateRequest[] = [
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'winDayOfWeek',
        filter: 'winnerName eq "Provium"',
        select: ['avgSolveTime'],
    },
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'solveTime(5, minute)',
        filter: 'abandoned eq false and solveTime lt 14400',
    },
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'hostName',
        filter: 'winnerName eq "Provium"',
        sort: ['numRounds desc'],
    },
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'winTimeOfDay(1, hour)',
        filter: 'hostName eq "Provium"',
    },
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'winTime(1, month)',
        select: ['numWinners', 'numHosts'],
    },
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'winTime(1, month)',
        select: ['minSolveTime'],
    },
    {
        endpoint: 'rounds/aggregate',
        groupBy: 'winnerName',
        select: ['avgSolveTime'],
        sort: ['avgSolveTime asc'],
        limit: 50,
        filter: 'winTime gte 2022-01-01 and winTime lt 2023-01-01',
    },
];

const charts: ChartQuery[] = [
    { chart: barChart, request: requests[0] },
    { chart: histogram, request: requests[1] },
    { chart: pieChart1, request: requests[2] },
    { chart: pieChart2, request: requests[3] },
    { chart: scatterPlot, request: requests[4] },
    { chart: timeSeries, request: requests[5] },
    { chart: table, request: requests[6] },
];

const ShowBuilder = false;
export default function SandboxPage() {
    if (ShowBuilder) {
        return <DummyBuilder />;
    }
    return <DummySandbox />;
}

function DummyBuilder() {
    const api = React.useContext(ApiContext);

    const getData = React.useCallback(async (params: SingleRoundsAggregateRequest) => {
        const res = await api.bulkRequest({ requests: [params] });
        return (res.responses[0] as ListResponse<RoundGroup>).results as unknown as ChartData[];
    }, [api]);

    return (
        <div className='page-viewport'>
            <div className='page-body'>
                <ChartBuilder
                    groupingFieldTypes={RoundGroupingFields}
                    groupingFieldTranslations={RoundFieldTranslations}
                    groupingFieldFilterSpec={RoundFieldSpec}
                    metricFieldTypes={RoundAggFields}
                    metricFieldTranslations={RoundAggFieldTranslations}
                    getData={getData}
                />
            </div>
        </div>
    );
}

function DummySandbox() {
    const api = React.useContext(ApiContext);

    const [data, setData] = React.useState<ChartData[][] | null>(null);
    React.useEffect(() => {
        if (data) {
            return;
        }

        // XNX needs to be redone when charts are a prop + leak if the component unmounts while request is in flight
        async function fetchData() {
            const res = await api.bulkRequest({ requests: charts.map(c => c.request) });
            setData(res.responses.map(((r: ListResponse<RoundGroup>) => r.results as unknown as ChartData[])));
        }
        fetchData();
    }, [data, api]);

    const zoomProps = useZoomState();

    return (
        <div className='sandbox-charts'>
            {charts.map((c, i) => <CustomChart
                key={i}
                data={data?.[i] ?? []}
                loaded={!!data}
                chartConfig={c.chart}
                groupingFieldTypes={RoundGroupingFields}
                groupingFieldTranslations={RoundFieldTranslations}
                metricFieldTypes={RoundAggFields}
                metricFieldTranslations={RoundAggFieldTranslations}
                {...zoomProps}
            />)}
        </div>
    );
}
