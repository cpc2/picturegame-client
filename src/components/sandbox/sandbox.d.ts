import { SingleRequest } from 'picturegame-api-wrapper';

import { ChartConfig } from '../custom_charts/CustomChartHelper';

export interface SandboxSpec {
    requests: SingleRequest[];
    layout: SandboxSection[];
}

export interface SandboxSection {
    header: string;
    widgets: Widget[];
}

export interface Widget {
    requestIndex: number;
    chart: ChartConfig;
}
