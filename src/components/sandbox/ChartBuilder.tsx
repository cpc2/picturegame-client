import { SingleRoundsAggregateRequest } from 'picturegame-api-wrapper';
import * as React from 'react';

import { useAsyncData } from '../../hooks/Promise';
import { FieldTranslations } from '../../model/Translation';
import { useZoomState, FieldValueType, ValueTypeNames } from '../../utils/ChartUtils';
import { classNameFromObj, getColour } from '../../utils/CssUtils';

import { Button } from '../base/Button';
import { ColourPicker } from '../base/ColourPicker';
import { Dropdown } from '../base/Dropdown';
import { RadioButtonGroup } from '../base/RadioButton';
import { Filter } from '../filter/Filter';
import { FilterSpec } from '../filter/FilterUtils';
import { FieldSelector } from '../misc/FieldSelector';

import { CustomChart } from '../custom_charts/CustomChart';
import {
    extractBaseChartConfig,
    ChartConfig, ChartData, CustomChartType, FieldTypeMap, LegendType, SeriesConfig,
} from '../custom_charts/CustomChartHelper';

import { initBarChart, isBarChart, BarChartConfig } from '../custom_charts/BarChart';
import { initPieChart, isPieChart, PieChartConfig } from '../custom_charts/PieChart';
import { initScatterPlot, isScatterPlot, ScatterPlotConfig } from '../custom_charts/ScatterPlotChart';
import { initTable } from '../custom_charts/Table';
import { initTimeSeries, isTimeSeries, SeriesConfigTimeSeries, TimeSeriesConfig } from '../custom_charts/TimeSeriesChart';

export enum GroupType {
    Invalid,
    ExactMatch,
    AbsoluteTime,
    RelativeTime,
    Duration,
}

const FieldGroupTypes: Readonly<Record<FieldValueType, GroupType>> = {
    [FieldValueType.Username]: GroupType.ExactMatch,
    [FieldValueType.DayOfWeek]: GroupType.ExactMatch,
    [FieldValueType.Date]: GroupType.AbsoluteTime,
    [FieldValueType.TimeOfDay]: GroupType.RelativeTime,
    [FieldValueType.Duration]: GroupType.Duration,
    [FieldValueType.NumRounds]: GroupType.Invalid,
    [FieldValueType.NumPlayers]: GroupType.Invalid,
};

type GroupTimeUnit = 'second' | 'minute' | 'hour' | 'day' | 'week' | 'month' | 'year';
const GroupTypeTimeUnits: { [K in GroupType]?: GroupTimeUnit[] } = {
    [GroupType.AbsoluteTime]: ['year', 'month', 'week', 'day', 'hour'],
    [GroupType.RelativeTime]: ['hour', 'minute'],
    [GroupType.Duration]: ['hour', 'minute', 'second'],
};

const ValidChartTypes: Readonly<Record<GroupType, ReadonlyArray<CustomChartType>>> = {
    [GroupType.Invalid]: [],
    [GroupType.ExactMatch]: [CustomChartType.Bar, CustomChartType.Pie, CustomChartType.Scatter, CustomChartType.Table],
    [GroupType.AbsoluteTime]: [CustomChartType.TimeSeries, CustomChartType.Scatter],
    [GroupType.RelativeTime]: [CustomChartType.Histogram, CustomChartType.Pie, CustomChartType.Scatter],
    [GroupType.Duration]: [CustomChartType.Histogram, CustomChartType.Pie, CustomChartType.Scatter],
};

const ChartTypeNames: Readonly<Record<CustomChartType, string>> = {
    [CustomChartType.Bar]: 'Bar Chart',
    [CustomChartType.Histogram]: 'Histogram',
    [CustomChartType.Pie]: 'Pie Chart',
    [CustomChartType.Scatter]: 'Scatter Plot',
    [CustomChartType.TimeSeries]: 'Time Series',
    [CustomChartType.Table]: 'Table',
    [CustomChartType.WordCloud]: 'Word Cloud',
};

export interface ChartBuilderProps<TElem, TGroup> {
    groupingFieldTypes: FieldTypeMap<TElem>;
    groupingFieldTranslations: FieldTranslations<TElem>;
    groupingFieldFilterSpec: FilterSpec<TElem>;

    metricFieldTypes: FieldTypeMap<TGroup>;
    metricFieldTranslations: FieldTranslations<TGroup>;

    getData(params: SingleRoundsAggregateRequest): Promise<ChartData[]>;
}

export function ChartBuilder<TElem, TGroup>(props: ChartBuilderProps<TElem, TGroup>) {
    const [request, setRequest] = React.useState<SingleRoundsAggregateRequest>({
        endpoint: 'rounds/aggregate',
        select: Object.keys(props.metricFieldTypes),
    });

    const [config, setConfig] = React.useState<ChartConfig>();

    const { data, requestPending } = useAsyncData(props.getData, request);
    const zoomProps = useZoomState();

    return <div className='chart-builder-cont'>
        {config && <CustomChart
            className='chart-builder-section'
            data={data}
            loaded={!requestPending && !!data?.length}
            {...zoomProps}
            chartConfig={config}
            groupingFieldTranslations={props.groupingFieldTranslations}
            groupingFieldTypes={props.groupingFieldTypes}
            metricFieldTranslations={props.metricFieldTranslations}
            metricFieldTypes={props.metricFieldTypes}
        />}
        <RequestBuilder
            groupingFieldFilterSpec={props.groupingFieldFilterSpec}
            groupingFieldTranslations={props.groupingFieldTranslations}
            groupingFieldTypes={props.groupingFieldTypes}
            metricFieldTranslations={props.metricFieldTranslations}
            metricFieldTypes={props.metricFieldTypes}
            request={request}
            setRequest={setRequest}
        />
        <ChartConfigBuilder
            groupingFieldFilterSpec={props.groupingFieldFilterSpec}
            groupingFieldTranslations={props.groupingFieldTranslations}
            groupingFieldTypes={props.groupingFieldTypes}
            metricFieldTranslations={props.metricFieldTranslations}
            metricFieldTypes={props.metricFieldTypes}
            data={data}
            request={request}
            config={config}
            setConfig={setConfig}
        />
    </div>;
}

const GroupByArgRegex = /^([a-zA-Z]+)(?:\((.*)\))?$/;

interface RequestBuilderProps<TElem, TGroup> {
    groupingFieldTypes: FieldTypeMap<TElem>;
    groupingFieldTranslations: FieldTranslations<TElem>;
    groupingFieldFilterSpec: FilterSpec<TElem>;

    metricFieldTypes: FieldTypeMap<TGroup>;
    metricFieldTranslations: FieldTranslations<TGroup>;

    request: SingleRoundsAggregateRequest;
    setRequest: React.Dispatch<React.SetStateAction<SingleRoundsAggregateRequest>>;
}

function RequestBuilder<TElem, TGroup>(props: RequestBuilderProps<TElem, TGroup>) {
    const allFieldTranslations = React.useMemo(
        () => ({ ...props.groupingFieldTranslations, ...props.metricFieldTranslations }),
        [props.groupingFieldTranslations, props.metricFieldTranslations]);

    const groupBy = parseGroupBy(props.request.groupBy);
    const validSortFields = React.useMemo(() => {
        const metricFieldNames = Object.keys(props.metricFieldTypes) as (keyof TGroup)[];
        return groupBy.fieldName ? [groupBy.fieldName as keyof TElem, ...metricFieldNames] : metricFieldNames;
    }, [groupBy.fieldName, props.metricFieldTypes]);

    return <div className='chart-builder-section'>
        <header>Request parameters</header>
        <div className='chart-builder-section-body'>
            <div className='chart-builder-row-label'>
                Filter
            </div>
            <Filter
                fieldTranslations={props.groupingFieldTranslations}
                filterSpec={props.groupingFieldFilterSpec}
                filterString={props.request.filter ?? ''}
                setFilterString={filter => props.setRequest(req => ({ ...req, filter }))}
                invalid={false}
            />
            <div className='chart-builder-row-label'>
                Group by
                </div>
            <GroupSelector
                fieldTypes={props.groupingFieldTypes}
                fieldTranslations={props.groupingFieldTranslations}
                groupBy={groupBy}
                onChange={next => props.setRequest(req => ({ ...req, groupBy: formatGroupBy(next) }))}
            />
            <div className='chart-builder-row-label'>
                Sort
                </div>
            <FieldSelector<TElem & TGroup>
                fieldTranslations={allFieldTranslations}
                value={props.request.sort?.[0]}
                onChange={sort => props.setRequest(req => ({ ...req, sort: sort === groupBy.fieldName ? undefined : [sort] }))}
                validFields={validSortFields}
                placeholder={groupBy.fieldName && props.groupingFieldTranslations[groupBy.fieldName as keyof TElem]}
            />
        </div>
    </div>;
}

interface GroupBy {
    fieldName?: string;
    interval?: number;
    unit?: GroupTimeUnit;
}

interface GroupSelectorProps<T> {
    fieldTypes: FieldTypeMap<T>;
    fieldTranslations: FieldTranslations<T>;

    groupBy: GroupBy;
    onChange(nextValue: GroupBy): void;
}

function parseGroupBy(groupBy?: string): GroupBy {
    const groupByMatch = groupBy?.match(GroupByArgRegex);
    const fieldName = groupByMatch?.[1];
    const args = groupByMatch?.[2]?.split(',').map(v => v.trim()) ?? [];
    const interval = parseInt(args[0], 10);

    return {
        fieldName,
        interval: isNaN(interval) ? undefined : interval,
        unit: args[1] as GroupTimeUnit,
    };
}

function formatGroupBy({ fieldName, interval, unit }: GroupBy) {
    return (fieldName && interval) ? `${fieldName}(${interval}, ${unit})` : fieldName;
}

function GroupSelector<T>(props: GroupSelectorProps<T>) {
    const { fieldName, interval, unit } = props.groupBy;

    const validGroupFields = React.useMemo(() => Object.keys(props.fieldTypes) as (keyof T)[],
        [props.fieldTypes]);

    const fieldType: FieldValueType | undefined = props.fieldTypes[fieldName as keyof T];
    const groupType = fieldType && FieldGroupTypes[fieldType];
    const timeUnits = typeof groupType === 'number' ? GroupTypeTimeUnits[groupType] : undefined;

    React.useEffect(() => {
        if (!fieldName) {
            return;
        }

        if (timeUnits) {
            if (!timeUnits.includes(unit!)) {
                props.onChange({ fieldName, interval: 1, unit: timeUnits[0] });
            }
        } else if (unit) {
            props.onChange({ fieldName });
        }
    }, [timeUnits, unit, fieldName, props.onChange]);

    return <div className='group-selector-cont'>
        <FieldSelector
            placeholder='Select a field (required)'
            fieldTranslations={props.fieldTranslations}
            validFields={validGroupFields}
            value={fieldName}
            onChange={val => props.onChange({ fieldName: val, interval, unit })}
        />
        {timeUnits && <div className='interval-selector-cont'>
            <div className='chart-builder-row-label'>Interval:</div>
            <input
                type='number'
                className='interval-selector'
                value={interval ?? '1'}
                onChange={ev => props.onChange({ fieldName, interval: Math.max(1, parseInt(ev.target.value, 10)), unit })}
                min={1}
            />
            <Dropdown
                className='unit-selector'
                value={unit ?? ''}
                suggestions={timeUnits}
                getSuggestionValue={(v: GroupTimeUnit) => v}
                renderSuggestion={(v: GroupTimeUnit) => <div>{v}</div>}
                onSubmit={v => props.onChange({ fieldName, interval, unit: v as GroupTimeUnit })}
                showChevron
            />
        </div>}
    </div>;
}

interface ChartConfigBuilderProps<TElem, TGroup> {
    groupingFieldTypes: FieldTypeMap<TElem>;
    groupingFieldTranslations: FieldTranslations<TElem>;
    groupingFieldFilterSpec: FilterSpec<TElem>;

    metricFieldTypes: FieldTypeMap<TGroup>;
    metricFieldTranslations: FieldTranslations<TGroup>;

    request: SingleRoundsAggregateRequest;
    data?: ChartData[];

    config?: ChartConfig;
    setConfig: React.Dispatch<React.SetStateAction<ChartConfig | undefined>>;
}

function initChartConfig(chartType: CustomChartType): ChartConfig {
    switch (chartType) {
        case CustomChartType.Bar:
        case CustomChartType.Histogram:
            return initBarChart(chartType);
        case CustomChartType.Pie:
            return initPieChart();
        case CustomChartType.Scatter:
            return initScatterPlot();
        case CustomChartType.TimeSeries:
            return initTimeSeries();
        case CustomChartType.Table:
            return initTable();
        default:
            throw new Error(`Unexpected chart type: ${chartType}`);
    }
}

function ChartConfigBuilder<TElem, TGroup>(props: ChartConfigBuilderProps<TElem, TGroup>) {
    const groupBy = parseGroupBy(props.request.groupBy);
    const groupFieldType = props.groupingFieldTypes[groupBy.fieldName as keyof TElem];
    const groupType = FieldGroupTypes[groupFieldType] ?? GroupType.Invalid;
    const validChartTypes = ValidChartTypes[groupType];

    // XNX - would like to reinstate previous config when changing chart types

    function onChangeChartType(nextType: CustomChartType) {
        const nextConfig: ChartConfig = {
            ...initChartConfig(nextType),
            ...(props.config ? extractBaseChartConfig(props.config) : {}),
            chartType: nextType,
            primaryKey: groupBy.fieldName!,
        };

        props.setConfig(nextConfig);
    }

    function renderChartTypeOption(chartType: CustomChartType) {
        const activeOption = props.config?.chartType === chartType;
        const validOption = validChartTypes.includes(chartType);
        const className = classNameFromObj({
            'chart-type-option': true,
            'active': activeOption,
            'invalid': !validOption,
        });

        return <div
            className={className}
            onClick={() => validOption && !activeOption && onChangeChartType(chartType)}
            title={validOption ? undefined : 'This chart type is not valid for the current grouping.'}
        >
            {ChartTypeNames[chartType]}
        </div>;
    }

    React.useEffect(() => {
        if (props.config && groupBy.fieldName && props.config.primaryKey !== groupBy.fieldName) {
            props.setConfig({ ...props.config, primaryKey: groupBy.fieldName });
        }
    }, [props.config, groupBy.fieldName]);

    React.useEffect(() => {
        if (props.config?.chartType && !validChartTypes.includes(props.config.chartType)) {
            props.setConfig(undefined);
        }
    }, [props.config?.chartType, validChartTypes]);

    return <div className='chart-builder-section'>
        <header>Design your chart</header>
        <div className='chart-builder-section-body'>
            <div className='chart-builder-row-label'>
                Chart type
            </div>
            <div className='chart-type-options'>
                {renderChartTypeOption(CustomChartType.Bar)}
                {renderChartTypeOption(CustomChartType.Histogram)}
                {renderChartTypeOption(CustomChartType.Pie)}
                {renderChartTypeOption(CustomChartType.Scatter)}
                {renderChartTypeOption(CustomChartType.TimeSeries)}
                {renderChartTypeOption(CustomChartType.Table)}
            </div>
            {props.config && <>
                <div className='chart-builder-row-label'>
                    Title
                </div>
                <div className='chart-builder-row-body'>
                    <input
                        type='text'
                        value={props.config?.header ?? ''}
                        placeholder='Chart Title'
                        onChange={ev => props.setConfig({ ...props.config!, header: ev.target.value })}
                    />
                </div>
                {isBarChart(props.config) && <BarChartConfigBuilder {...props} config={props.config} />}
                {isPieChart(props.config) && <PieChartConfigBuilder {...props} config={props.config} />}
                {isScatterPlot(props.config) && <ScatterPlotConfigBuilder {...props} config={props.config} />}
                {isTimeSeries(props.config) && <TimeSeriesConfigBuilder {...props} config={props.config} />}
                <div className='chart-builder-row-label'>
                    Legend
            </div>
                <LegendSelector
                    legendType={props.config?.legendType}
                    setLegendType={nextType => props.setConfig({ ...props.config!, legendType: nextType })}
                />
            </>}
        </div>
    </div>;
}

interface LegendSelectorProps {
    legendType?: LegendType;
    setLegendType(nextType?: LegendType): void;
}

function LegendSelector(props: LegendSelectorProps) {
    const options = ['None', 'Bottom', 'Right'];

    function currentOption() {
        switch (props.legendType) {
            case 'bottom': return 1;
            case 'right': return 2;
            default: return 0;
        }
    }

    function getLegendType(index: number): LegendType | undefined {
        switch (index) {
            case 1: return 'bottom';
            case 2: return 'right';
            default: return undefined;
        }
    }

    return <RadioButtonGroup
        options={options}
        selectedIndex={currentOption()}
        onChange={idx => props.setLegendType(getLegendType(idx))}
    />;
}

function calculateIntervalSec(groupBy: GroupBy) {
    if (!groupBy.interval || !groupBy.unit) {
        return undefined;
    }

    switch (groupBy.unit) {
        case 'hour':
            return groupBy.interval * 3600;
        case 'minute':
            return groupBy.interval * 60;
        case 'second':
            return groupBy.interval;
        default:
            return undefined;
    }
}

function calculateSeriesWarnings<T>(series: SeriesConfig[], fieldTypes: FieldTypeMap<T>): (string | undefined)[] {
    const seriesValueTypes = series.map(s => fieldTypes[s.key as keyof T]);

    const yAxisFirstSeriesIdx = series.findIndex(s => (s.axis ?? 1) === 1);
    const yAxisValueType = seriesValueTypes[yAxisFirstSeriesIdx];

    const yAxis2FirstSeriesIdx = series.findIndex(s => s.axis === 2);
    const yAxis2ValueType = seriesValueTypes[yAxis2FirstSeriesIdx];

    return series.map(s => {
        const axisType = s.axis === 2 ? yAxis2ValueType : yAxisValueType;
        const seriesType = fieldTypes[s.key as keyof T];

        if (axisType !== seriesType) {
            return `The unit for this series (${ValueTypeNames[seriesType]}) ` +
                `is incompatible with the chosen Y axis (${ValueTypeNames[axisType]}).`;
        }
    });
}

interface BarChartConfigBuilderProps<TElem, TGroup> extends ChartConfigBuilderProps<TElem, TGroup> {
    config: BarChartConfig;
    setConfig: React.Dispatch<React.SetStateAction<BarChartConfig>>;
}

function BarChartConfigBuilder<TElem, TGroup>(props: BarChartConfigBuilderProps<TElem, TGroup>) {
    const validFields = React.useMemo(() => Object.keys(props.metricFieldTypes) as (keyof TGroup)[], [props.metricFieldTypes]);

    const groupBy = parseGroupBy(props.request.groupBy);
    const groupInterval = calculateIntervalSec(groupBy);

    React.useEffect(() => {
        if (props.config.chartType === CustomChartType.Histogram) {
            props.setConfig(prev => ({ ...prev, interval: groupInterval }));
        }
    }, [props.config.chartType, groupInterval]);

    function onChangeSeries<K extends keyof SeriesConfig>(seriesPatch: Pick<SeriesConfig, K>, index: number) {
        const nextSeries = [...props.config.series];
        nextSeries[index] = {
            ...nextSeries[index],
            ...seriesPatch,
        };
        props.setConfig({ ...props.config, series: nextSeries });
    }

    function onAddSeries() {
        const nextSeries = [...props.config.series];
        const defaultKey = validFields[0];
        nextSeries.push({
            key: defaultKey as string,
            colour: getColour(nextSeries.length),
        });

        props.setConfig({ ...props.config, series: nextSeries });
    }

    function onDeleteSeries(index: number) {
        const nextSeries = [...props.config.series];
        nextSeries.splice(index, 1);
        props.setConfig({ ...props.config, series: nextSeries });
    }

    const seriesWarnings = calculateSeriesWarnings(props.config.series, props.metricFieldTypes);

    return <>
        <div className='chart-builder-row-label'>
            Series
        </div>
        <div className='series-entry-cont'>
            {props.config.series.map((series, i) => <div key={i} className='series-row'>
                <ColourPicker
                    colour={series.colour ?? 'black'}
                    setColour={nextColour => onChangeSeries({ colour: nextColour }, i)}
                />
                <div className='series-config'>
                    <FieldSelector
                        fieldTranslations={props.metricFieldTranslations}
                        value={series.key}
                        validFields={validFields}
                        onChange={val => onChangeSeries({ key: val }, i)}
                    />
                    <RadioButtonGroup
                        label='Y axis'
                        options={['Left', 'Right']}
                        selectedIndex={(series.axis ?? 1) - 1}
                        onChange={index => onChangeSeries({ axis: index + 1 }, i)}
                    />
                </div>
                {seriesWarnings[i] && <i className='fas fa-exclamation-triangle series-warning' title={seriesWarnings[i]} />}
                <Button title='Delete series' onClick={() => onDeleteSeries(i)}>
                    <i className='fas fa-trash' />
                </Button>
            </div>)}
            <Button className='add-series-button' onClick={onAddSeries}>
                <span>Add Series</span>
                <i className='fas fa-plus' />
            </Button>
        </div>
    </>;
}

interface PieChartConfigBuilderProps<TElem, TGroup> extends ChartConfigBuilderProps<TElem, TGroup> {
    config: PieChartConfig;
    setConfig: React.Dispatch<React.SetStateAction<PieChartConfig>>;
}

function PieChartConfigBuilder<TElem, TGroup>(props: PieChartConfigBuilderProps<TElem, TGroup>) {
    const validFields = React.useMemo(() => Object.keys(props.metricFieldTypes) as (keyof TGroup)[], [props.metricFieldTypes]);

    return <>
        <div className='chart-builder-row-label'>
            Series
        </div>
        <FieldSelector
            fieldTranslations={props.metricFieldTranslations}
            value={props.config.secondaryKey}
            validFields={validFields}
            onChange={val => props.setConfig({ ...props.config, secondaryKey: val })}
        />
        <div className='chart-builder-row-label'>
            Combine
        </div>
        <div className='chart-builder-row-body'>
            <span>Entries less than</span>
            <input
                type='number'
                className='pie-percent-selector'
                min={0}
                max={100}
                value={props.config.combineLessThanPercent ?? ''}
                onChange={ev => {
                    const nextValue = parseInt(ev.target.value, 10);
                    props.setConfig({ ...props.config, combineLessThanPercent: isNaN(nextValue) ? undefined : nextValue });
                }}
            />
            <span>%</span>
        </div>
    </>;
}

interface ScatterPlotConfigBuilderProps<TElem, TGroup> extends ChartConfigBuilderProps<TElem, TGroup> {
    config: ScatterPlotConfig;
    setConfig: React.Dispatch<React.SetStateAction<ScatterPlotConfig>>;
}

function ScatterPlotConfigBuilder<TElem, TGroup>(props: ScatterPlotConfigBuilderProps<TElem, TGroup>) {
    const validFields = React.useMemo(() => Object.keys(props.metricFieldTypes) as (keyof TGroup)[], [props.metricFieldTypes]);

    return <>
        <div className='chart-builder-row-label'>
            X Axis
        </div>
        <FieldSelector
            fieldTranslations={props.metricFieldTranslations}
            value={props.config.xKey}
            validFields={validFields}
            onChange={val => props.setConfig({ ...props.config, xKey: val })}
        />
        <div className='chart-builder-row-label'>
            Y Axis
        </div>
        <FieldSelector
            fieldTranslations={props.metricFieldTranslations}
            value={props.config.yKey}
            validFields={validFields}
            onChange={val => props.setConfig({ ...props.config, yKey: val })}
        />
        <div className='chart-builder-row-label'>
            Colour
        </div>
        <ColourPicker
            colour={props.config.colour ?? 'black'}
            setColour={colour => props.setConfig({ ...props.config, colour })}
        />
    </>;
}

interface TimeSeriesConfigBuilderProps<TElem, TGroup> extends ChartConfigBuilderProps<TElem, TGroup> {
    config: TimeSeriesConfig;
    setConfig: React.Dispatch<React.SetStateAction<TimeSeriesConfig>>;
}

function TimeSeriesConfigBuilder<TElem, TGroup>(props: TimeSeriesConfigBuilderProps<TElem, TGroup>) {
    const validFields = React.useMemo(() => Object.keys(props.metricFieldTypes) as (keyof TGroup)[], [props.metricFieldTypes]);

    function onChangeSeries<K extends keyof SeriesConfigTimeSeries>(seriesPatch: Pick<SeriesConfigTimeSeries, K>, index: number) {
        const nextSeries = [...props.config.series];
        nextSeries[index] = {
            ...nextSeries[index],
            ...seriesPatch,
        };
        props.setConfig({ ...props.config, series: nextSeries });
    }

    function onAddSeries() {
        const nextSeries = [...props.config.series];
        const defaultKey = validFields[0];
        nextSeries.push({
            key: defaultKey as string,
            colour: getColour(nextSeries.length),
            display: 'line',
        });

        props.setConfig({ ...props.config, series: nextSeries });
    }

    function onDeleteSeries(index: number) {
        const nextSeries = [...props.config.series];
        nextSeries.splice(index, 1);
        props.setConfig({ ...props.config, series: nextSeries });
    }

    const seriesWarnings = calculateSeriesWarnings(props.config.series, props.metricFieldTypes);

    return <>
        <div className='chart-builder-row-label'>
            Series
        </div>
        <div className='series-entry-cont'>
            {props.config.series.map((series, i) => <div key={i} className='series-row'>
                <ColourPicker
                    colour={series.colour ?? 'black'}
                    setColour={nextColour => onChangeSeries({ colour: nextColour }, i)}
                />
                <div className='series-config'>
                    <FieldSelector
                        fieldTranslations={props.metricFieldTranslations}
                        value={series.key}
                        validFields={validFields}
                        onChange={val => onChangeSeries({ key: val }, i)}
                    />
                    <RadioButtonGroup
                        label='Y axis'
                        options={['Left', 'Right']}
                        selectedIndex={(series.axis ?? 1) - 1}
                        onChange={index => onChangeSeries({ axis: index + 1 }, i)}
                    />
                    <RadioButtonGroup
                        label='Display'
                        options={['Line', 'Bar']}
                        selectedIndex={series.display === 'line' ? 0 : 1}
                        onChange={index => onChangeSeries({ display: index === 0 ? 'line' : 'bar' }, i)}
                    />
                </div>
                {seriesWarnings[i] && <i className='fas fa-exclamation-triangle series-warning' title={seriesWarnings[i]} />}
                <Button title='Delete series' onClick={() => onDeleteSeries(i)}>
                    <i className='fas fa-trash' />
                </Button>
            </div>)}
            <Button className='add-series-button' onClick={onAddSeries}>
                <span>Add Series</span>
                <i className='fas fa-plus' />
            </Button>
        </div>
    </>;
}
