import * as API from 'picturegame-api-wrapper';
import * as React from 'react';

import * as UI from '../../model/ui';

import { useQueryArray, QueryParam } from '../../hooks/History';
import { useDocumentTitle } from '../../hooks/Misc';
import { useState } from '../../hooks/Redux';

import { useZoomState } from '../../utils/ChartUtils';

import { LoadingState } from '../base/LoadingState';
import { RankChart } from '../charts/RankChart';
import { RoundsChart } from '../charts/RoundsChart';
import { WeeklyWinsChart } from '../charts/WeeklyWinsChart';

import { LeaderboardFilter } from '../leaderboard/LeaderboardFilter';
import { OverviewTable } from './OverviewTable';

import './Compare.scss';

function extractState(state: UI.State, usernames: string[]) {
    return {
        activeUsers: (usernames || [])
            .map(u => state.data.leaderboard.get(u.toLowerCase()))
            .filter(p => !!p) as API.LeaderboardEntry[],
        leaderboardLoaded: state.leaderboard.dataLoaded,
        chartVisibility: state.userConfig.chartVisibility,
    };
}

export default function ComparisonPage() {
    const [playerNames, setPlayerNames] = useQueryArray(QueryParam.Players);
    const { activeUsers, leaderboardLoaded, chartVisibility } = useState(extractState, playerNames);

    const zoomProps = useZoomState();

    useDocumentTitle(() => 'Compare Players', []);

    if (!playerNames || !playerNames.length) {
        return null;
    }

    if (!activeUsers.length) {
        return !leaderboardLoaded ? <LoadingState /> : renderPlayerNotFound();
    }

    function addPlayerName(username: string) {
        setPlayerNames([...new Set([...playerNames!, username])]);
    }

    function removePlayerName(username: string) {
        username = username.toLowerCase();
        setPlayerNames(playerNames!.filter(n => n.toLowerCase() !== username));
    }

    return (<div className='page-viewport'>
        <div className='page-body'>
            <LeaderboardFilter />
            <OverviewTable
                activePlayers={activeUsers}
                addActivePlayer={addPlayerName}
                removeActivePlayer={removePlayerName}
            />
            <div className='compare-charts'>
                {chartVisibility.rounds && <RoundsChart activePlayers={activeUsers} {...zoomProps} />}
                {chartVisibility.ranks && <RankChart activePlayers={activeUsers} {...zoomProps} />}
                {chartVisibility.weekly_wins && <WeeklyWinsChart activePlayers={activeUsers} {...zoomProps} />}
            </div>
        </div>
    </div>);
}

function renderPlayerNotFound() {
    return (<div className='user-error-state'>
        No such player
    </div>);
}
