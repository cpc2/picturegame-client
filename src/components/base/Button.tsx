import * as React from 'react';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import './Button.scss';

export interface ButtonProps extends ClassStyleProps {
    onClick?: (ev: React.MouseEvent<HTMLDivElement>) => void;
    disabled?: boolean;
    label?: string;
    title?: string;
    active?: boolean;
    primary?: boolean;
}

export class Button extends React.PureComponent<ButtonProps> {
    onClick = (ev: React.MouseEvent<HTMLDivElement>) => {
        if (!this.props.disabled) {
            this.props.onClick?.(ev);
        }
    }

    render() {
        const classStyleProps = combineProps(this.props, {
            'button': true,
            'disabled': !!this.props.disabled,
            'active': !!this.props.active,
            'primary': !!this.props.primary,
        });

        return <div {...classStyleProps} onClick={this.onClick} title={this.props.title}>
            {this.props.label ?? this.props.children}
        </div>;
    }
}
