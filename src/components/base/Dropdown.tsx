import * as React from 'react';

import { useCancellableAsync } from '../../hooks/Promise';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import './Dropdown.scss';

export interface DropdownProps<TSuggestion> extends ClassStyleProps {
    value: string;
    onChange?: (value: string) => void;
    onBlur?: (event: React.FormEvent<any>) => void;
    onSubmit?: (value: string) => void;
    onKeyDown?: (ev: React.KeyboardEvent<HTMLInputElement>) => void;
    placeholder?: string;
    suggestions: TSuggestion[];
    getSuggestionValue(suggestion: TSuggestion): string;
    onUpdateSuggestions?: import('react-autosuggest').SuggestionsFetchRequested;
    onClearSuggestions?(): void;
    renderSuggestion(suggestion: TSuggestion): React.ReactNode;
    disabled?: boolean;
    showChevron?: boolean;
}

export function Dropdown<TSuggestion>(props: DropdownProps<TSuggestion>) {
    const [importAutosuggest, setAutosuggest] = React.useState<any>(null);
    useCancellableAsync(() => import(/* webpackChunkName: "react-autosuggest" */'react-autosuggest'), setAutosuggest);

    if (!importAutosuggest) {
        return renderFallbackInput(props);
    }

    const Autosuggest = importAutosuggest.default as typeof import('react-autosuggest');

    return <Autosuggest
        getSuggestionValue={props.getSuggestionValue}
        inputProps={{
            className: props.className,
            style: props.style,
            value: props.value,
            disabled: props.disabled,
            readOnly: !props.onChange,
            onChange: (_, params) => props.onChange && props.onChange(params ? params.newValue : ''),
            onBlur: props.onBlur,
            placeholder: props.placeholder,
            onKeyPress: (ev) => {
                if (props.onSubmit && ev.key === 'Enter') {
                    props.onSubmit(props.value);
                }
            },
            onKeyDown: props.onKeyDown,
        }}
        onSuggestionsFetchRequested={props.onUpdateSuggestions ?? (() => { })}
        onSuggestionsClearRequested={props.onClearSuggestions ?? (() => { })}
        onSuggestionSelected={(_, params) => props.onSubmit && props.onSubmit(params.suggestionValue)}
        renderSuggestion={props.renderSuggestion}
        suggestions={props.suggestions}
        shouldRenderSuggestions={() => true}
        focusInputOnSuggestionClick={false}
        renderInputComponent={inputProps => (
            <div {...combineProps(props, 'dropdown-cont')}>
                <input {...inputProps as any} />
                {props.showChevron && <i className='fas fa-chevron-down' />}
            </div>
        )}
    />;
}

function renderFallbackInput(props: DropdownProps<unknown>) {
    return <input
        type='text'
        className={props.className}
        placeholder={props.placeholder}
        style={props.style}
        value={props.value}
        readOnly={!props.onChange}
        onChange={ev => props.onChange && props.onChange(ev.target.value)}
    />;
}
