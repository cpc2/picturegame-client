import { debounce } from 'lodash-es';
import * as React from 'react';

import { ClassStyleProps } from '../../utils/CssUtils';

export interface EllipsisProps extends ClassStyleProps {
    text: string;
}

export function Ellipsize(props: EllipsisProps) {
    const elRef = React.useRef<HTMLDivElement>(null);
    const containerRef = React.useRef<HTMLDivElement>(null);

    const textRef = React.useRef(props.text);
    if (textRef.current !== props.text) {
        textRef.current = props.text;
    }

    function processSize() {
        const el = elRef.current;
        const container = containerRef.current;
        const text = textRef.current;

        if (!el || !container) { return; }

        el.innerHTML = text;
        if (el.offsetHeight <= container.offsetHeight) {
            return;
        }

        el.innerHTML = getText(text, searchForMaxLength(el, container, text, 0, text.length - 1));
    }

    React.useEffect(() => {
        const debounceProcess = debounce(processSize, 200);
        window.addEventListener('resize', debounceProcess);
        return () => window.removeEventListener('resize', debounceProcess);
    }, []);

    React.useEffect(() => { processSize(); });

    return (
        <div
            style={props.style}
            className={props.className}
            ref={containerRef}
        >
            <div ref={elRef}>{props.text}</div>
        </div>
    );
}

function searchForMaxLength(el: HTMLElement, container: HTMLElement, text: string, min: number, max: number): number {
    const testLength = Math.floor((min + max) / 2);

    if (testOverflow(el, container, getText(text, testLength))) {
        // Overflows with this length, recurse on the shorter half
        return searchForMaxLength(el, container, text, min, testLength - 1);
    }
    if (testOverflow(el, container, getText(text, testLength + 1))) {
        // Doesn't overflow at this length but does with one more char, we found the max
        return testLength;
    }
    // Doesn't overflow at this length, we can fit more
    return searchForMaxLength(el, container, text, testLength + 1, max);
}

function testOverflow(el: HTMLElement, container: HTMLElement, text: string) {
    el.innerHTML = text;
    return el.offsetHeight > container.offsetHeight;
}

function getText(text: string, length: number) {
    return `${text.substr(0, length)}...`;
}
