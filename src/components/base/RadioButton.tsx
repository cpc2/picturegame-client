import * as React from 'react';

import { classNameFromObj, combineProps, ClassStyleProps } from '../../utils/CssUtils';

import './RadioButton.scss';

export interface RadioButtonProps extends ClassStyleProps {
    label?: string;
    options: string[];
    selectedIndex?: number;
    onChange(nextIndex: number): void;
}

export function RadioButtonGroup(props: RadioButtonProps) {
    function renderOption(optionText: string, index: number) {
        const active = index === props.selectedIndex;

        const optionClassName = classNameFromObj({
            'radio-button-option': true,
            'active': active,
        });

        return <div
            key={index}
            className={optionClassName}
            onClick={() => !active && props.onChange(index)}
        >
            <i className={active ? 'far fa-dot-circle' : 'far fa-circle'} />
            <span className='option-text'>{optionText}</span>
        </div>;
    }

    return <div {...combineProps(props, 'radio-button-group')}>
        {props.label && <div className='radio-buttons-label'>{props.label}:</div>}
        {props.options.map(renderOption)}
    </div>;
}
