import * as React from 'react';

import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import './ColourPicker.scss';

export interface ColourPickerProps extends ClassStyleProps {
    label?: string;
    colour: string;
    setColour(nextColour: string): void;
}

export function ColourPicker(props: ColourPickerProps) {
    const colourInputRef = React.useRef<HTMLInputElement>(null);

    return <div {...combineProps(props, 'colour-picker-cont')}>
        {props.label && <div className='colour-picker-label'>{props.label}:</div>}
        <input
            type='color'
            value={props.colour}
            onChange={ev => props.setColour(ev.target.value)}
            style={{ display: 'none' }}
            ref={colourInputRef}
        />
        <div
            className='colour-swatch'
            onClick={() => colourInputRef.current?.click()}
            style={{ backgroundColor: props.colour }}>
            <i className='fas fa-eye-dropper' />
        </div>
    </div>;
}
