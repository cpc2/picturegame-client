import * as React from 'react';

export function LoadingState() {
    return (<div className='loading-state'>
        <div className='loader' />
    </div>);
}
