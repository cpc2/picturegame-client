import { ArrayExpression, BaseExpression, IToken, TokenType, ValidOperators } from 'picturegame-api-wrapper';

import * as React from 'react';

import { useState } from '../../hooks/Redux';

import { FieldTranslations } from '../../model/Translation';
import { State } from '../../model/ui';
import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import { ArrayPicker } from '../base/ArrayPicker';
import { Button } from '../base/Button';
import { FieldSelector } from '../misc/FieldSelector';

import { CompOperatorTranslation, FieldSpec, FilterSpec, FormatFilter, IsArrayOperator, ParseFilter } from './FilterUtils';
import { FormatArray, FormatRaw } from './ValueFormatters';
import { EnumPicker, EnumPickerOption, SystemInput } from './ValueInputs';

import './Filter.scss';

const NewFilterId = '$$NEW_FILTER$$';

export interface FilterProps<T> extends ClassStyleProps {
    fieldTranslations: FieldTranslations<T>;
    filterSpec: FilterSpec<T>;
    filterString: string;
    setFilterString(filter?: string): void;
    invalid?: boolean;
}

export function Filter<T>(props: FilterProps<T>) {
    const [filter, setFilter] = React.useState(ParseFilter(''));
    const [parseFailed, setParseFailed] = React.useState(false);
    React.useEffect(() => {
        try {
            setFilter(ParseFilter(props.filterString));
            setParseFailed(false);
        } catch (e) {
            setFilter(ParseFilter(''));
            setParseFailed(true);
        }
    }, [props.filterString]);

    const [editingClauseIdx, setEditingClauseIdx] = React.useState<string | null>(null);
    const [showAdvanced, setShowAdvanced] = React.useState(false);

    function submitClause(id: string, newClause: BaseExpression) {
        const newClauses = filter.set(id, newClause);
        props.setFilterString(FormatFilter(newClauses));
        setEditingClauseIdx(null);
    }

    function deleteClause(id: string) {
        const newClauses = filter.delete(id);
        props.setFilterString(FormatFilter(newClauses));

        if (editingClauseIdx === id) {
            setEditingClauseIdx(null);
        }
    }

    return <div {...combineProps(props, 'filter-cont')}>
        {!parseFailed && <div className='filter-chips-cont'>
            {filter.map((clause, i) => <FilterChip
                key={i}
                clause={clause}
                onClick={() => setEditingClauseIdx(i)}
                onDelete={() => deleteClause(i)}
                fieldTranslation={props.fieldTranslations[clause.lValue.input as keyof T]}
                fieldSpec={props.filterSpec[clause.lValue.input as keyof T]}
                active={i === editingClauseIdx}
            />).valueSeq()}
            <Button
                className='add-filter-button'
                onClick={() => setEditingClauseIdx(NewFilterId)}
            >
                <span>Add a filter</span><i className='fas fa-plus' />
            </Button>
            <div className='advanced-filter-toggle'>
                <Button onClick={() => setShowAdvanced(v => !v)}>{showAdvanced ? 'Hide' : 'Show'} advanced filter</Button>
            </div>
        </div>}
        {parseFailed && <div className='filter-chips-cont'>
            <i className='fas fa-exclamation-circle highlight-warning icon' />
            <span>The query you entered is either invalid or too complex to display.</span>
        </div>}
        {editingClauseIdx !== null && <FilterEditor
            clause={filter.get(editingClauseIdx)}
            fieldTranslations={props.fieldTranslations}
            filterSpec={props.filterSpec}
            onSubmit={(clause) => submitClause(editingClauseIdx, clause)}
            onDelete={() => deleteClause(editingClauseIdx)}
            onCancel={() => setEditingClauseIdx(null)}
        />}
        {(showAdvanced || parseFailed) && <TextFilter
            filterString={props.filterString}
            setFilterString={props.setFilterString}
            invalid={props.invalid}
        />}
    </div>;
}

interface TextFilterProps {
    filterString: string;
    setFilterString(filter?: string): void;
    invalid?: boolean;
}

function extractState(state: State) {
    return {
        apiUrl: state.config.apiUrl,
    };
}

function TextFilter(props: TextFilterProps) {
    const [inputFilter, setInputFilter] = React.useState(props.filterString);
    const { apiUrl } = useState(extractState);

    React.useEffect(() => {
        setInputFilter(props.filterString);
    }, [props.filterString]);

    return <div className='filter-text-input'>
        <input
            type='text'
            value={inputFilter}
            onChange={ev => setInputFilter(ev.target.value)}
            placeholder='Enter a filter query'
            onKeyPress={ev => {
                if (ev.key === 'Enter') {
                    props.setFilterString(inputFilter || undefined);
                }
            }}
            className={props.invalid ? 'invalid' : ''}
        />
        <Button onClick={() => props.setFilterString(inputFilter || undefined)}>
            Filter
        </Button>
        <a href={`${apiUrl}/docs/#/guides/filters`} target='_blank'>Help</a>
    </div>;
}

interface FilterChipProps {
    fieldTranslation: string;
    fieldSpec: FieldSpec;
    clause: BaseExpression;
    onDelete(): void;
    onClick(): void;
    active: boolean;
}

function FilterChip(props: FilterChipProps) {
    const { lValue, operator, rValue } = props.clause;

    // Assume validation has been done before rendering this
    const formatFn = props.fieldSpec?.rValueChipFormat ?? FormatRaw;
    const formattedValue = rValue instanceof ArrayExpression
        ? FormatArray(rValue, formatFn)
        : formatFn(rValue);

    function onClickChip(ev: React.MouseEvent<HTMLDivElement>) {
        if (!ev.defaultPrevented) {
            props.onClick();
        }
    }

    function onClickDelete(ev: React.MouseEvent<HTMLDivElement>) {
        ev.preventDefault();
        props.onDelete();
    }

    const compOperatorTransFn = CompOperatorTranslation.get(operator.input);
    const valueType = props.fieldSpec?.valueType ?? TokenType.Invalid;
    const compOperatorText = compOperatorTransFn ? compOperatorTransFn(valueType) : operator.input;

    return <div className='filter-chip-cont'>
        <div className={'filter-chip' + (props.active ? ' active' : '')} onClick={onClickChip} >
            <div className='filter-chip-text'>
                <span className='field-name'>{props.fieldTranslation ?? lValue.input}</span>
                <span className='operator'>{compOperatorText}</span>
                <span className='value'>{formattedValue}</span>
            </div>
            <i className='fas fa-times delete-button' onClick={onClickDelete} />
        </div>
    </div>;
}

interface FilterEditorProps<T> {
    fieldTranslations: FieldTranslations<T>;
    filterSpec: FilterSpec<T>;
    clause?: BaseExpression;
    onSubmit(newClause: BaseExpression): void;
    onDelete(): void;
    onCancel(): void;
}

function FilterEditor<T>(props: FilterEditorProps<T>) {
    const [lValue, setLValue] = React.useState(props.clause?.lValue);
    const [operator, setOperator] = React.useState(props.clause?.operator);
    const [rValue, setRValue] = React.useState(props.clause?.rValue);

    React.useEffect(() => {
        setLValue(props.clause?.lValue);
        setOperator(props.clause?.operator);
        setRValue(props.clause?.rValue);
    }, [props.clause]);

    const fieldSpec = props.filterSpec[lValue?.input as keyof T];
    const valid = !!fieldSpec && !!operator && !!rValue && (!(rValue instanceof ArrayExpression) || rValue.items.length > 0);

    const fieldType = fieldSpec?.valueType ?? TokenType.Invalid;
    const allOperators = React.useMemo<EnumPickerOption[]>(
        () => [...ValidOperators[fieldType]].map(o => ({ value: o, translation: CompOperatorTranslation.get(o)!(fieldType) })),
        [fieldType]);
    const OperatorSelector = React.useMemo(() => EnumPicker(allOperators), [allOperators]);

    const RValueInput = React.useMemo(() => fieldSpec?.rValueInputComponent ?? SystemInput(), [fieldSpec?.rValueInputComponent]);

    function onChangeLValue(newValue: string) {
        if (newValue !== lValue?.input) {
            const newFieldSpec = props.filterSpec[newValue as keyof T];
            setLValue({ index: 0, input: newValue, type: TokenType.Identifier });

            const currentOperator = newFieldSpec.defaultOperator ?? ValidOperators[newFieldSpec.valueType]?.values().next().value;
            setOperator(currentOperator ? { index: 0, input: currentOperator, type: TokenType.CompOperator } : undefined);

            if (!newFieldSpec.defaultValue) {
                setRValue(undefined);
            }

            if (newFieldSpec.defaultValue && !IsArrayOperator(currentOperator)) {
                setRValue({ index: 0, input: newFieldSpec.defaultValue, type: newFieldSpec.valueType });
            }
        }
    }

    function onChangeOperator(newValue: string) {
        if (newValue !== operator?.input) {
            setOperator({ index: 0, input: newValue, type: TokenType.CompOperator });

            if (operator && IsArrayOperator(newValue) !== IsArrayOperator(operator.input)) {
                setRValue(undefined);
            }
        }
    }

    function onChangeRValue(newValue: string) {
        if (newValue) {
            setRValue({ index: 0, input: newValue, type: fieldSpec.valueType });
        } else {
            setRValue(undefined);
        }
    }

    function onChangeRValueArray(newValues: string[]) {
        const expression = new ArrayExpression(0);
        newValues = [...new Set(newValues)];
        expression.items = newValues.map(v => ({ index: 0, input: v, type: fieldSpec.valueType }));
        setRValue(expression);
    }

    const useArrayPicker = IsArrayOperator(operator?.input);

    return <div className='filter-editor-cont'>
        <h3>
            {props.clause ? 'Edit' : 'Add'} Filter
        </h3>
        <div className='filter-editor-body'>
            <FieldSelector
                fieldTranslations={props.fieldTranslations}
                value={lValue?.input}
                onChange={onChangeLValue}
            />
            <OperatorSelector
                value={operator?.input ?? ''}
                onChange={onChangeOperator}
                placeholder='Operator'
                className='operator-selector'
                disabled={!fieldSpec}
            />
            {!useArrayPicker && <RValueInput
                className='value-selector'
                placeholder='Enter a value'
                disabled={!operator}
                value={(rValue as IToken)?.input ?? ''}
                onChange={onChangeRValue}
            />}
        </div>
        {useArrayPicker && <ArrayPicker
            values={((rValue as ArrayExpression)?.items ?? []).map(i => i.input)}
            setValues={onChangeRValueArray}
            className='filter-selector value-selector'
            inputComp={RValueInput}
            placeholder='Tab/Enter to add values'
            formatChip={val => fieldSpec.rValueChipFormat({ index: 0, input: val, type: fieldSpec.valueType })}
        />}
        <div className='filter-buttons-row button-row'>
            <Button
                primary
                disabled={!valid}
                onClick={() => props.onSubmit(new BaseExpression(lValue!, operator!, rValue!))}
            >
                {props.clause ? 'Save' : 'Add'}
            </Button>
            <Button onClick={props.onCancel}>Cancel</Button>
        </div>
    </div>;
}
