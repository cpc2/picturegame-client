import * as Immutable from 'immutable';
import * as Api from 'picturegame-api-wrapper';

import { ValueInputProps } from './ValueInputs';

export type ActiveFilter = Immutable.OrderedMap<string, Api.BaseExpression>;

export interface FieldSpec {
    valueType: Api.ValueType;
    rValueChipFormat(input: Api.IToken): string;
    rValueInputComponent(props: ValueInputProps): JSX.Element;
    defaultValue?: string;
    defaultOperator?: Api.CompOperatorType;
}

export type FilterSpec<T> = Record<keyof Required<T>, FieldSpec>;

export function FormatFilter(filter: ActiveFilter): string {
    return filter.map(FormatClause).join(' and ');
}

export function FormatClause(clause: Api.BaseExpression): string {
    return `${clause.lValue.input} ${clause.operator.input} ${formatRValue(clause.rValue)}`;
}

function formatRValue(rValue: Api.RValueType): string {
    if (rValue instanceof Api.ArrayExpression) {
        return '[' + rValue.items.map(formatRValue).join(', ') + ']';
    }

    if (rValue.type === Api.TokenType.StringValue) {
        return '"' + rValue.input + '"';
    }

    return rValue.input;
}

export function ParseFilter(filterString: string): ActiveFilter {
    if (filterString.length === 0) {
        return Immutable.OrderedMap<string, Api.BaseExpression>();
    }

    const tokens = new Api.Tokenizer(filterString).tokenize();
    const expressionTree = new Api.ExpressionTreeBuilder(tokens).read();

    const clausesM = Immutable.OrderedMap<string, Api.BaseExpression>().asMutable();
    addClauses(expressionTree, clausesM);
    return clausesM.asImmutable();
}

function addClauses(expressionTree: Api.ExpressionTree, clauses: ActiveFilter) {
    if (expressionTree instanceof Api.BaseExpression) {
        clauses.set(FormatClause(expressionTree), expressionTree);
    } else if (expressionTree instanceof Api.CompoundExpression) {
        if (expressionTree.operator.input !== Api.LogicalOperatorType.And) {
            throw new Api.QueryParseError('Filtering UI only supports "and" combinators');
        }
        addClauses(expressionTree.lValue, clauses);
        addClauses(expressionTree.rValue, clauses);
    } else {
        throw new Api.QueryParseError('Unexpected expression type');
    }
}

export interface CompOperatorTranslationFn {
    (valueType: Api.ValueType): string;
}

export const CompOperatorTranslation: ReadonlyMap<string, CompOperatorTranslationFn> = new Map<string, CompOperatorTranslationFn>([
    [Api.CompOperatorType.Equal, () => 'is'],
    [Api.CompOperatorType.NotEqual, () => 'is not'],
    [Api.CompOperatorType.Greater, valueType => IsTimeValue(valueType) ? 'is after' : 'is greater than'],
    [Api.CompOperatorType.GreaterOrEqual, () => 'is at least'],
    [Api.CompOperatorType.Less, valueType => IsTimeValue(valueType) ? 'is before' : 'is less than'],
    [Api.CompOperatorType.LessOrEqual, () => 'is at most'],
    [Api.CompOperatorType.Contains, () => 'contains'],
    [Api.CompOperatorType.NotContains, () => 'does not contain'],
    [Api.CompOperatorType.In, () => 'is one of'],
    [Api.CompOperatorType.NotIn, () => 'is not any of'],
]);

export function IsArrayOperator(operator?: string) {
    return operator === Api.CompOperatorType.In || operator === Api.CompOperatorType.NotIn;
}

export function IsTimeValue(valueType?: Api.ValueType) {
    return valueType === Api.TokenType.TimeValue || valueType === Api.TokenType.DateTimeValue;
}

export type SortDir = 'asc' | 'desc';
export type SortSpec<T> = [keyof T, SortDir];
export function GetSortSpec<T>(rawSort: string | null | undefined, spec: FilterSpec<T>, defaultField: keyof T): SortSpec<T> {
    if (!rawSort) {
        return [defaultField, 'asc'];
    }

    const tokens = rawSort.split(' ');
    const fieldTok = tokens[0] as keyof T;
    if (!spec[fieldTok]) {
        return [defaultField, 'asc'];
    }

    if (tokens.length > 1 && (tokens[1] === 'asc' || tokens[1] === 'desc')) {
        return [fieldTok, tokens[1]];
    }

    return [fieldTok, 'asc'];
}
