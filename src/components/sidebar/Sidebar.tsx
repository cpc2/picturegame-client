import * as React from 'react';

import { SetTheme, ToggleChartVisibility } from '../../actions/UserConfigActions';
import { usePath } from '../../hooks/History';
import { useDispatch, useState } from '../../hooks/Redux';
import * as UI from '../../model/ui';
import { classNameFromObj } from '../../utils/CssUtils';

import { Button } from '../base/Button';
import { CheckBox } from '../base/CheckBox';
import { Link } from '../base/Router';

import './Sidebar.scss';

export interface SidebarProps {
    open: boolean;
    onClose: () => void;
}

function extractState(state: UI.State) {
    return {
        theme: state.userConfig.theme ?? 'dark',
    };
}

function extractDispatch(dispatch: UI.DispatchAction) {
    return {
        setTheme: (theme: UI.ThemeSetting) => dispatch(SetTheme(theme)),
    };
}

export function Sidebar(props: SidebarProps) {
    const sidebarRef = React.useRef<HTMLDivElement>(null);
    useFocus(sidebarRef, props.onClose);

    const { theme } = useState(extractState);
    const { setTheme } = useDispatch(extractDispatch);

    const className = classNameFromObj({
        'sidebar-cont': true,
        'sidebar-open': props.open,
    });

    return (
        <div
            className={className}
            ref={sidebarRef}
        >
            <header>
                <Button onClick={props.onClose}>
                    <i className='fas fa-times' />
                </Button>
            </header>
            <div className='sidebar-body'>
                <SidebarNavigation />
                <div className='sidebar-section'>
                    <h3>Theme</h3>
                    <div className='sidebar-theme-options'>
                        <div
                            className={'sidebar-option' + (theme === 'dark' ? ' selected' : '')}
                            onClick={() => setTheme('dark')}>
                            <i className='fas fa-moon' /> Dark
                        </div>
                        <div
                            className={'sidebar-option' + (theme === 'light' ? ' selected' : '')}
                            onClick={() => setTheme('light')}>
                            <i className='fas fa-sun' /> Light
                        </div>
                    </div>
                </div>
                <ChartToggle />
            </div>
        </div>
    );
}

function useFocus<T extends HTMLElement>(ref: React.RefObject<T>, onClose: () => void) {
    const onWindowClick = React.useCallback((ev: MouseEvent) => {
        if (ev.defaultPrevented || !ref.current) { return; }

        if (!ref.current.contains(ev.target as HTMLElement)) {
            onClose();
        }
    }, [onClose]);

    React.useEffect(() => {
        window.addEventListener('click', onWindowClick);
        return () => window.removeEventListener('click', onWindowClick);
    }, [onWindowClick]);
}

function SidebarNavigation() {
    const currentPath = usePath();

    function renderNavLink(text: string, path: string) {
        if (path === currentPath) {
            return <div className='sidebar-option selected'>{text}</div>;
        }

        return <Link path={path}>
            {linkProps => (
                <div className='sidebar-option'>
                    <a {...linkProps}>{text}</a>
                </div>
            )}
        </Link>;
    }

    return <div className='sidebar-section'>
        <h3>Navigation</h3>
        {renderNavLink('Leaderboard', '/')}
        {renderNavLink('Round Explorer', '/rounds')}
        {renderNavLink('Facts and Figures', '/facts')}
    </div>;
}

function extractChartToggleState(state: UI.State) {
    return {
        chartVisibility: state.userConfig.chartVisibility,
    };
}

function extractChartToggleDispatch(dispatch: UI.DispatchAction) {
    return {
        toggleChart: (chart: UI.ChartId) => dispatch(ToggleChartVisibility(chart)),
    };
}

function ChartToggle() {
    const { chartVisibility } = useState(extractChartToggleState);
    const { toggleChart } = useDispatch(extractChartToggleDispatch);

    function renderOption(chartId: UI.ChartId, name: string) {
        return <div className='sidebar-option sidebar-toggle-row'>
            <div className='sidebar-toggle-name'>{name}</div>
            <CheckBox checked={chartVisibility[chartId]} onToggle={() => toggleChart(chartId)} />
        </div>;
    }

    return <div className='sidebar-section'>
        <h3>Charts</h3>
        <div className='sidebar-chart-toggles'>
            {renderOption('rounds', 'Wins over time')}
            {renderOption('ranks', 'Rank over time')}
            {renderOption('weekly_wins', 'Weekly wins')}
        </div>
    </div>;
}
