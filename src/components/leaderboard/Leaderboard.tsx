import * as API from 'picturegame-api-wrapper';
import * as React from 'react';
import * as Reselect from 'reselect';

import { useQueryString, QueryParam } from '../../hooks/History';
import { useDocumentTitle } from '../../hooks/Misc';
import { useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import { classNameFromObj } from '../../utils/CssUtils';
import { getInt } from '../../utils/QueryUtils';

import { ColumnSpec, DataGrid } from '../base/DataGrid';
import { Link } from '../base/Router';

import { LeaderboardFilter } from './LeaderboardFilter';

import './Leaderboard.scss';

const columns: ColumnSpec<API.LeaderboardEntry>[] = [
    {
        id: 'rank',
        name: 'Rank',
        cell: p => <td className='col-numeric'>{p.rank}</td>,
        width: 56,
    },
    {
        id: 'username',
        name: 'Username',
        cell: p => (<td>
            <Link path='/dashboard' query={{ player: p.username }}>
                {props => <a {...props}>{p.username}</a>}
            </Link>
        </td>),
        style: { minWidth: 80 },
        width: '1fr',
    },
    {
        id: 'numWins',
        name: 'Wins',
        cell: p => <td className='col-numeric'>{p.numWins}</td>,
        width: 80,
    },
];

function stateToProps(state: UI.State, page: number) {
    return {
        players: getSinglePagePlayers(state, page),
        fullLeaderboard: state.data.leaderboard,
        playersPerPage: state.leaderboard.itemsPerPage,
        jumpedPlayer: state.leaderboard.jumpedPlayer,
        leaderboardLoaded: state.leaderboard.dataLoaded,
    };
}

const getSortedPlayers: (state: UI.State) => API.LeaderboardEntry[] = Reselect.createSelector(
    (state: UI.State) => state.data.leaderboard,
    function _getSortedPlayers(players): API.LeaderboardEntry[] {
        return players.sort((a, b) => (a.rank as number) - (b.rank as number)).valueSeq().toArray();
    });

const getSinglePagePlayers: (state: UI.State, page: number) => API.LeaderboardEntry[] = Reselect.createSelector(
    getSortedPlayers,
    (_state: UI.State, page: number) => page,
    (state: UI.State) => state.leaderboard.itemsPerPage,
    function _getSinglePagePlayers(players, currentPage, itemsPerPage): API.LeaderboardEntry[] {
        const startIndex = currentPage * itemsPerPage;
        return players.slice(startIndex, startIndex + itemsPerPage);
    });

export default function Leaderboard() {
    const [rawPage, setPage] = useQueryString(QueryParam.Page);
    const page = getInt(rawPage, 0);

    useDocumentTitle(() => {
        if (page === 0) {
            return 'Leaderboard';
        }
        return `Leaderboard - Page ${page + 1}`;
    }, [page]);

    const stateProps = useState(stateToProps, page);


    const maxPage = Math.max(0, Math.ceil(stateProps.fullLeaderboard.size / stateProps.playersPerPage) - 1);

    function renderPlayer(player: API.LeaderboardEntry, cells: React.ReactElement<any>[]) {
        const active = stateProps.jumpedPlayer && player.username.toLowerCase() === stateProps.jumpedPlayer;
        const className = classNameFromObj({
            'jump-player': !!active,
        });
        return (<tr className={className} key={player.username}>
            {cells}
        </tr>);
    }

    return (<div className='leaderboard-container'>
        <LeaderboardFilter className='lb-page-filter' />
        <DataGrid
            heading='Leaderboard'
            columns={columns}
            data={stateProps.players}
            className='leaderboard-table-cont'
            rowRenderer={renderPlayer}
            currentPage={page}
            maxPage={maxPage}
            onChangePage={setPage}
            emptyState={<EmptyState dataLoaded={stateProps.leaderboardLoaded} />}
        />
    </div>);
}

interface EmptyStateProps {
    dataLoaded: boolean;
}

function EmptyState({ dataLoaded }: EmptyStateProps) {
    return <div className='empty-state'>
        {dataLoaded && <span>Nobody has won a round yet for this date range.</span>}
        {!dataLoaded && <div className='loader' />}
    </div>;
}
