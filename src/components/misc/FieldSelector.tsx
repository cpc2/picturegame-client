import * as React from 'react';

import { useSyncedState } from '../../hooks/Misc';
import { FieldTranslations } from '../../model/Translation';
import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import { Dropdown } from '../base/Dropdown';

export interface FieldSelectorProps<T> extends ClassStyleProps {
    fieldTranslations: FieldTranslations<T>;
    validFields?: (keyof T)[];
    value?: string;
    onChange(value: string): void;
    placeholder?: string;
}

export function FieldSelector<T>(props: FieldSelectorProps<T>) {
    const [inputValue, setInputValue] = useSyncedState(() => props.fieldTranslations[props.value as keyof T] ?? props.value ?? '',
        [props.fieldTranslations, props.value]);

    const [invalid, setInvalid] = React.useState(false);

    const sortedFieldNames = React.useMemo(() =>
        (props.validFields ?? Object.keys(props.fieldTranslations) as (keyof T)[])
            .sort((ka, kb) => props.fieldTranslations[ka] < props.fieldTranslations[kb] ? -1 : 1),
        [props.validFields, props.fieldTranslations]);

    const allFieldTranslations = React.useMemo(
        () => new Set<string>(sortedFieldNames.map(name => props.fieldTranslations[name as keyof T])),
        [sortedFieldNames, props.fieldTranslations]);

    const [suggestions, setSuggestions] = React.useState<(keyof T)[]>(sortedFieldNames);
    const onUpdateSuggestions: import('react-autosuggest').SuggestionsFetchRequested = React.useCallback((params) => {
        if (allFieldTranslations.has(params.value)) {
            // Selected a value; reset the suggestions so the user can easily change their selection
            setSuggestions(sortedFieldNames);
            return;
        }

        const input = params.value.toLowerCase().trim();
        const nextSuggestions: (keyof T)[] = [];
        for (const fieldName of sortedFieldNames) {
            if ((fieldName as string).toLowerCase().includes(input) || props.fieldTranslations[fieldName].toLowerCase().includes(input)) {
                nextSuggestions.push(fieldName);
            }
        }
        setSuggestions(nextSuggestions);
    }, [props.fieldTranslations, sortedFieldNames]);

    const onClearSuggestions = React.useCallback(() => setSuggestions(sortedFieldNames), [sortedFieldNames]);

    function renderSuggestion(suggestion: keyof T) {
        return <div>{props.fieldTranslations[suggestion]}</div>;
    }

    function onSubmit(value: string) {
        if (!value) {
            return;
        }

        const valueLower = value.toLowerCase().trim();
        for (const key in props.fieldTranslations) {
            if (key.toLowerCase() === valueLower || props.fieldTranslations[key].toLowerCase() === valueLower) {
                props.onChange(key);
                setInputValue(props.fieldTranslations[key]);
                setInvalid(false);
                return;
            }
        }

        setInvalid(true);
    }

    return <Dropdown
        value={inputValue}
        onChange={setInputValue}
        placeholder={props.placeholder ?? 'Select a field'}
        suggestions={suggestions}
        onUpdateSuggestions={onUpdateSuggestions}
        onClearSuggestions={onClearSuggestions}
        getSuggestionValue={fieldName => props.fieldTranslations[fieldName]}
        renderSuggestion={renderSuggestion}
        onSubmit={onSubmit}
        {...combineProps(props, {
            'filter-selector': true,
            'field-selector': true,
            'invalid': invalid,
        })}
        showChevron
    />;
}
