import * as React from 'react';

import { useState } from '../../hooks/Redux';
import { combineProps, ClassStyleProps } from '../../utils/CssUtils';
import { getStringMatch, StringMatch } from '../../utils/StringUtils';

import * as UI from '../../model/ui';

import { Dropdown } from '../base/Dropdown';

const MaxUsernameLength = 20;

interface Props extends ClassStyleProps {
    value?: string;
    onChange?: (value: string) => void;
    onSubmit?: (username: string) => void;
    onKeyDown?: (ev: React.KeyboardEvent<HTMLInputElement>) => void;
    placeholder?: string;
    disabled?: boolean;
    clearOnSubmit?: boolean;
}

function stateToProps(state: UI.State) {
    return {
        players: state.data.leaderboard,
    };
}

export function PlayerSearch(props: Props) {
    const [value, setValue] = React.useState('');
    const { players } = useState(stateToProps);

    const playerNames = React.useMemo(() => players.map(p => p.username).valueSeq().toArray(), [players]);
    const [suggestions, setSuggestions] = React.useState<StringMatch[]>([]);

    const onUpdateSuggestions: import('react-autosuggest').SuggestionsFetchRequested = React.useCallback((params) => {
        const nextSuggestions = getFilteredSuggestions(playerNames, params.value);
        setSuggestions(nextSuggestions.slice(0, 10));
    }, [playerNames]);

    const onClearSuggestions = React.useCallback(() => {
        setSuggestions([]);
    }, []);

    const onSubmit = (name: string) => {
        if (props.onSubmit) {
            name = name.toLowerCase();
            if (players.has(name)) {
                props.onSubmit(name);
            }
        }
        if (props.clearOnSubmit) {
            setValue('');
        }
    };

    return <div {...combineProps(props, 'player-search-container row-content')}>
        <Dropdown
            value={props.value ?? value}
            onChange={props.onChange ?? setValue}
            onSubmit={onSubmit}
            onKeyDown={props.onKeyDown}
            placeholder={props.placeholder}
            suggestions={suggestions}
            onUpdateSuggestions={onUpdateSuggestions}
            onClearSuggestions={onClearSuggestions}
            getSuggestionValue={v => v.value}
            renderSuggestion={renderSuggestion}
            disabled={props.disabled}
        />
    </div>;
}

function getFilteredSuggestions(values: string[], input: string): StringMatch[] {
    input = input.toLowerCase().trim();
    const matches: StringMatch[] = [];
    for (const v of values) {
        const match = getStringMatch(v, input, MaxUsernameLength);
        if (match) {
            matches.push(match);
        }
    }
    return matches.sort((a, b) => b.score - a.score);
}

function renderSuggestion(match: StringMatch) {
    return <div>
        {match.tokens.map((t, i) => {
            if (t.matching) {
                return <strong key={i}>{t.value}</strong>;
            }
            return t.value;
        })}
    </div>;
}
