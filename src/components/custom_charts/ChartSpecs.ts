import { Round, RoundAggregates } from 'picturegame-api-wrapper';

import { FieldTypeMap } from './CustomChartHelper';

import { FieldValueType } from '../../utils/ChartUtils';

export const RoundGroupingFields: Partial<FieldTypeMap<Round>> = {
    hostName: FieldValueType.Username,
    winnerName: FieldValueType.Username,
    postDayOfWeek: FieldValueType.DayOfWeek,
    winDayOfWeek: FieldValueType.DayOfWeek,
    postTime: FieldValueType.Date,
    winTime: FieldValueType.Date,
    postTimeOfDay: FieldValueType.TimeOfDay,
    winTimeOfDay: FieldValueType.TimeOfDay,
    postDelay: FieldValueType.Duration,
    solveTime: FieldValueType.Duration,
    plusCorrectDelay: FieldValueType.Duration,
};

export const RoundAggFields: FieldTypeMap<RoundAggregates> = {
    numRounds: FieldValueType.NumRounds,
    minRoundNumber: FieldValueType.NumRounds,
    maxRoundNumber: FieldValueType.NumRounds,

    minPostTime: FieldValueType.Date,
    maxPostTime: FieldValueType.Date,

    minWinTime: FieldValueType.Date,
    maxWinTime: FieldValueType.Date,

    avgSolveTime: FieldValueType.Duration,
    minSolveTime: FieldValueType.Duration,
    maxSolveTime: FieldValueType.Duration,

    avgPostDelay: FieldValueType.Duration,
    minPostDelay: FieldValueType.Duration,
    maxPostDelay: FieldValueType.Duration,

    avgPlusCorrectDelay: FieldValueType.Duration,
    minPlusCorrectDelay: FieldValueType.Duration,
    maxPlusCorrectDelay: FieldValueType.Duration,

    numHosts: FieldValueType.NumPlayers,
    numWinners: FieldValueType.NumPlayers,
};
