import { LegendProps } from 'recharts';

import { FieldTranslations } from '../../model/Translation';
import { DataType, FieldValueType, ZoomableChartProps } from '../../utils/ChartUtils';
import { ClassStyleProps } from '../../utils/CssUtils';
import { moveToStartOfDay } from '../../utils/DateUtils';
import * as Formatter from '../../utils/Formatter';

export enum CustomChartType {
    Pie = 'pie',
    Bar = 'bar',
    Histogram = 'histogram',
    Scatter = 'scatter',
    TimeSeries = 'time',
    Table = 'table',
    WordCloud = 'wordcloud',
}

export type LegendType = 'bottom' | 'right';

// Data points should always be numbers, but sometimes the key might be a string (e.g. for pie chart).
// Can't figure out a way to make TS accept a single key being string, with everything else being number...
export type ChartValue = string | number;
export type ChartData = Record<string, ChartValue>;

export interface ChartConfig {
    header: string;
    chartType: CustomChartType;
    primaryKey: string;
    legendType?: LegendType;
    width?: number;
}

export function extractBaseChartConfig(config: ChartConfig): ChartConfig {
    return {
        header: config.header,
        chartType: config.chartType,
        primaryKey: config.primaryKey,
        legendType: config.legendType,
        width: config.width,
    };
}

export interface SeriesConfig {
    key: string;
    colour?: string;
    axis?: number;
}

export type FieldTypeMap<T> = { [K in keyof T]: FieldValueType };

export interface CustomChartProps<TElem, TGroup> extends ZoomableChartProps, ClassStyleProps {
    data?: ChartData[];
    loaded: boolean;
    chartConfig: ChartConfig;

    groupingFieldTypes: FieldTypeMap<TElem>;
    groupingFieldTranslations: FieldTranslations<TElem>;

    metricFieldTypes: FieldTypeMap<TGroup>;
    metricFieldTranslations: FieldTranslations<TGroup>;
}

export function getLegendProps(legendType: LegendType): LegendProps {
    if (legendType === 'bottom') {
        return {
            layout: 'horizontal',
            align: 'center',
            verticalAlign: 'bottom',
        };
    }

    return {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        wrapperStyle: {
            height: 'calc(100% - 41px)', // 21px top- and 20px bottom- margin on the chart
            overflowY: 'auto',
        },
    };
}

export function inferDataType(data: ChartData[] | undefined, field: string, valueType: FieldValueType): DataType {
    const firstVal = data?.[0]?.[field];

    if (typeof firstVal === 'string') {
        return 'string';
    }

    if (valueType === FieldValueType.Date) {
        // Precondition: data is sorted on the given field
        // Infer from looking at two consecutive data points whether the interval is months, years, or just generic dates
        const secondVal = data?.[1]?.[field];

        if (typeof firstVal !== 'number' || typeof secondVal !== 'number') {
            // Insufficient info to infer
            return 'date';
        }

        const firstDate = new Date(firstVal * 1000);
        moveToStartOfDay(firstDate);
        const secondDate = new Date(secondVal * 1000);
        moveToStartOfDay(secondDate);

        if (firstDate.getUTCDate() === 1 && secondDate.getUTCDate() === 1) {
            if (firstDate.getUTCMonth() === 0 && secondDate.getUTCMonth() === 0) {
                return 'year';
            }

            if (firstDate.getUTCFullYear() === secondDate.getUTCFullYear()) {
                return 'month';
            }

            return 'monthYear';
        }

        return 'date';
    }

    if (valueType === FieldValueType.NumPlayers || valueType === FieldValueType.NumRounds) {
        return 'number';
    }

    if (valueType === FieldValueType.Username) {
        return 'string';
    }

    return valueType;
}

export function formatValue(value: ChartValue, dataType?: DataType): string {
    if (typeof value === 'number') {
        switch (dataType) {
            case 'duration': return Formatter.formatDuration(value)!;
            case 'dayOfWeek': return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'][value];
            case 'timeOfDay': return Formatter.formatTimeOfDay(value);
            case 'date': return Formatter.formatDate(value)!;
            case 'month': return Formatter.formatMonth(value)!;
            case 'monthYear': return Formatter.formatMonthYear(value)!;
            case 'year': return Formatter.formatYear(value)!;
            case 'number': return value.toLocaleString();
        }
    }

    return `${value}`;
}
