import * as _ from 'lodash-es';
import * as React from 'react';

import {
    formatValue, getLegendProps, inferDataType,
    ChartConfig, ChartValue, CustomChartProps, CustomChartType,
} from './CustomChartHelper';

import { ChartTooltip } from '../charts/Chart';

import { getColour } from '../../utils/CssUtils';

export interface PieChartConfig extends ChartConfig {
    chartType: CustomChartType.Pie;
    secondaryKey: string;
    combineLessThanPercent?: number;
}
export function isPieChart(config: ChartConfig): config is PieChartConfig {
    return config.chartType === CustomChartType.Pie;
}

export function initPieChart(): PieChartConfig {
    return {
        chartType: CustomChartType.Pie,
        header: 'Chart Title',
        primaryKey: '',
        secondaryKey: '',
    };
}

interface PieChartProps<TElem, TGroup> {
    outerProps: CustomChartProps<TElem, TGroup>;
    config: PieChartConfig;
    Recharts: typeof import('recharts');
}
export function PieChartRenderer<TElem, TGroup>(
    { outerProps: props, config, Recharts, ...innerProps }: PieChartProps<TElem, TGroup>) {

    const { PieChart, Pie, Cell, Legend, Tooltip } = Recharts;

    const data = React.useMemo(() => {
        if (!config.combineLessThanPercent) {
            return props.data ?? [];
        }

        const values = props.data?.map(d => d[config.secondaryKey] as number) ?? [];
        const total = values.reduce((sum, next) => sum + next, 0);
        const min = total * config.combineLessThanPercent / 100;
        const totalExcluded = values.reduce((sum, next) => sum + (next < min ? next : 0), 0);
        if (totalExcluded) {
            return [
                ...(props.data ?? []).filter(d => d[config.secondaryKey] >= min),
                {
                    [config.primaryKey]: 'Other',
                    [config.secondaryKey]: totalExcluded,
                },
            ];
        }

        return props.data ?? [];
    }, [props.data, config.combineLessThanPercent, config.primaryKey, config.secondaryKey]);

    const primaryValueType = props.groupingFieldTypes[config.primaryKey as keyof TElem];
    const primaryDataType = inferDataType(props.data, config.primaryKey, primaryValueType);

    const secondaryValueType = props.metricFieldTypes[config.secondaryKey as keyof TGroup];
    const secondaryDataType = inferDataType(props.data, config.secondaryKey, secondaryValueType);

    const [legendHovered, setLegendHovered] = React.useState<ChartValue | null>(null);
    function onMouseEnter(payload: any) {
        setLegendHovered(payload.dataKey ?? payload.value);
    }
    function onMouseLeave() {
        setLegendHovered(null);
    }

    return <PieChart margin={{ top: 21, left: 20, bottom: 20, right: 20 }} {...innerProps}>
        <Pie
            data={data}
            nameKey={config.primaryKey}
            dataKey={config.secondaryKey}
        >
            {data.map((datum, index) => <Cell
                key={`cell-${datum[config.primaryKey]}`}
                fill={getColour(index)}
                opacity={(!_.isNil(legendHovered) && legendHovered !== datum[config.primaryKey]) ? 0.5 : 1}
            />)}
        </Pie>
        {config.legendType && <Legend
            {...getLegendProps(config.legendType)}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
            formatter={v => formatValue(v, primaryDataType)}
        />}
        <Tooltip
            content={<ChartTooltip nameFormatter={name => formatValue(name, primaryDataType)} />}
            formatter={val => formatValue(val as ChartValue, secondaryDataType)}
        />
    </PieChart>;
}
