import * as _ from 'lodash-es';
import * as React from 'react';
import { TooltipFormatter } from 'recharts';

import {
    formatValue, getLegendProps, inferDataType,
    ChartConfig, ChartData, ChartValue, CustomChartProps, CustomChartType, SeriesConfig,
} from './CustomChartHelper';

import { renderXLabel, renderYLabel, ChartTooltip } from '../charts/Chart';

import { calculateLinearTicks, getMaxValue, ValueTypeNames } from '../../utils/ChartUtils';

export interface BarChartConfig extends ChartConfig {
    chartType: CustomChartType.Bar | CustomChartType.Histogram;
    interval?: number; // For histogram only
    series: SeriesConfig[];
}
export function isBarChart(config: ChartConfig): config is BarChartConfig {
    return config.chartType === CustomChartType.Bar || config.chartType === CustomChartType.Histogram;
}

export function initBarChart(chartType: CustomChartType.Bar | CustomChartType.Histogram): BarChartConfig {
    return {
        chartType,
        header: 'Chart Title',
        primaryKey: '',
        series: [],
    };
}

interface BarChartProps<TElem, TGroup> {
    outerProps: CustomChartProps<TElem, TGroup>;
    config: BarChartConfig;
    containerRef: React.RefObject<HTMLDivElement>;
    Recharts: typeof import('recharts');
}
export function BarChartRenderer<TElem, TGroup>(
    { outerProps: props, config, containerRef, Recharts, ...innerProps }: BarChartProps<TElem, TGroup>) {

    const [legendHovered, setLegendHovered] = React.useState<ChartValue | null>(null);
    function onMouseEnter(payload: any) {
        setLegendHovered(payload.dataKey ?? payload.value);
    }
    function onMouseLeave() {
        setLegendHovered(null);
    }

    const xAxisValueType = props.groupingFieldTypes[config.primaryKey as keyof TElem];
    const xAxisDataType = inferDataType(props.data, config.primaryKey, xAxisValueType);

    const seriesValueTypes = config.series.map(s => props.metricFieldTypes[s.key as keyof TGroup]);
    const seriesDataTypes = config.series.map((s, i) => inferDataType(props.data, s.key, seriesValueTypes[i]));

    const yAxisFirstSeriesIdx = config.series.findIndex(series => (series.axis ?? 1) === 1);
    const yAxisValueType = seriesValueTypes[yAxisFirstSeriesIdx];
    const yAxisDataType = seriesDataTypes[yAxisFirstSeriesIdx];

    const yAxis2FirstSeriesIdx = config.series.findIndex(series => series.axis === 2);
    const needYAxis2 = yAxis2FirstSeriesIdx > -1;
    const yAxis2ValueType = seriesValueTypes[yAxis2FirstSeriesIdx];
    const yAxis2DataType = seriesDataTypes[yAxis2FirstSeriesIdx];

    const tooltipFormatter: TooltipFormatter = (value, _name, _payload, index) => {
        return formatValue(value as ChartValue, seriesDataTypes[index]);
    };

    // Insert zeros for empty bins if we're making a histogram
    const [data, xTicks] = React.useMemo((): [ChartData[], number[] | undefined] => {
        if (config.chartType === CustomChartType.Bar || !config.interval) {
            return [props.data ?? [], undefined];
        }

        // Precondition: For histogram data, the points must be sorted by their primary key
        const xMax = getMaxValue(props.data ?? [], d => [d[config.primaryKey] as number]);
        const ticks = calculateLinearTicks(0, xMax, xAxisDataType, config.interval);

        const points: typeof props.data = [];
        let i = 0;
        for (let x = 0; x <= xMax; x += config.interval) {
            if (props.data?.[i][config.primaryKey] === x) {
                points.push(props.data[i]);
                ++i;
            } else {
                const val = { [config.primaryKey]: x };
                for (const series of config.series) {
                    val[series.key] = 0;
                }
                points.push(val);
            }
        }

        return [points, ticks];
    }, [props.data, config.chartType, config.interval, config.primaryKey, xAxisDataType]);

    // Hack: reposition the x axis ticks for histogram so they are between the bars
    React.useEffect(() => {
        if (config.chartType === CustomChartType.Bar) {
            return;
        }

        const axisTicksCont = containerRef.current?.querySelector('.recharts-xAxis > .recharts-cartesian-axis-ticks');
        const axisLineCont = containerRef.current?.querySelector('.recharts-xAxis > .recharts-cartesian-axis-line');
        if (!axisTicksCont || !axisLineCont) {
            return;
        }

        const axisWidth = parseFloat(axisLineCont?.getAttribute('width') as string);
        if (!_.isNaN(axisWidth)) {
            const offset = -axisWidth / (2 * data.length);
            axisTicksCont.setAttribute('style', `transform: translateX(${offset}px)`);
        }
    });

    const ySeries = config.series.filter(s => s.axis !== 2);
    const yMax = getMaxValue(props.data ?? [], d => ySeries.map(s => d[s.key] as number));
    const yTicks = calculateLinearTicks(0, yMax, yAxisDataType);

    const y2Series = config.series.filter(s => s.axis === 2);
    const y2Max = getMaxValue(props.data ?? [], d => y2Series.map(s => d[s.key] as number));
    const y2Ticks = calculateLinearTicks(0, y2Max, yAxis2DataType);

    const { BarChart, XAxis, YAxis, Label, Bar, Legend, Tooltip } = Recharts;
    return <BarChart
        data={data}
        margin={{ top: 21, left: 20, bottom: 20, right: 20 }}
        barCategoryGap={config.chartType === CustomChartType.Histogram ? 0 : undefined}
        barGap={config.chartType === CustomChartType.Histogram ? 0 : undefined}
        {...innerProps}
    >
        <XAxis
            dataKey={config.primaryKey}
            tickFormatter={val => formatValue(val, xAxisDataType)}
            ticks={xTicks}
        >
            {renderXLabel(Label, props.groupingFieldTranslations[config.primaryKey as keyof TElem])}
        </XAxis>
        <YAxis
            yAxisId={1}
            ticks={yTicks}
            tickFormatter={val => formatValue(val, yAxisDataType)}
            domain={[0, yTicks[yTicks.length - 1]]}
        >
            {renderYLabel(Label, ValueTypeNames[yAxisValueType])}
        </YAxis>
        {needYAxis2 && <YAxis
            yAxisId={2}
            orientation='right'
            ticks={y2Ticks}
            tickFormatter={val => formatValue(val, yAxis2DataType)}
            domain={[0, y2Ticks[y2Ticks.length - 1]]}
        >
            {renderYLabel(Label, ValueTypeNames[yAxis2ValueType], 'right')}
        </YAxis>}
        {config.legendType && <Legend
            {...getLegendProps(config.legendType)}
            onMouseEnter={onMouseEnter}
            onMouseLeave={onMouseLeave}
        />}
        <Tooltip
            content={<ChartTooltip />}
            cursor={false}
            formatter={tooltipFormatter}
            labelFormatter={label => formatValue(label, xAxisDataType)}
        />
        {config.series.map(series => <Bar
            key={series.key}
            dataKey={series.key}
            name={props.metricFieldTranslations[series.key as keyof TGroup]}
            fill={series.colour}
            opacity={(legendHovered && legendHovered !== series.key) ? 0.5 : 1}
            yAxisId={series.axis ?? 1}
        />)}
    </BarChart>;
}
