import * as API from 'picturegame-api-wrapper';
import * as React from 'react';

import { UpdatePlayerMetrics } from '../../actions/DataActions';

import { fetchPlayerMetrics } from '../../api/Fetchers';

import { useApiQueryContext } from '../../hooks/Api';
import { useDateRange } from '../../hooks/Misc';
import { useDispatch, useState } from '../../hooks/Redux';
import * as UI from '../../model/ui';

import { convertToLeaderboardFilter } from '../../utils/DateUtils';
import { formatDuration } from '../../utils/Formatter';
import { formatRankPeakDates, getPlayerData } from '../../utils/LeaderboardUtils';

import { Link } from '../base/Router';

export interface PlayerOverviewProps {
    player: API.LeaderboardEntry;
}

function extractState(state: UI.State, player: API.LeaderboardEntry) {
    return {
        player: getPlayerData(state, [player])[0],
    };
}

function extractDispatch(dispatch: UI.DispatchAction) {
    return {
        onPlayerMetrics: (names: string[], metrics: API.Player[]) => dispatch(UpdatePlayerMetrics({ names, players: metrics })),
    };
}

// If the player is not found (either LB is still loading, or player doesn't exist), this component won't be mounted
export function PlayerOverview(props: PlayerOverviewProps) {
    const { onPlayerMetrics } = useDispatch(extractDispatch);
    const { player } = useState(extractState, props.player);

    const dateRange = useDateRange();
    const dateRangeFilter = convertToLeaderboardFilter(dateRange);
    const filterString = dateRangeFilter ? `${dateRangeFilter} and abandoned eq false` : 'abandoned eq false';

    const playerMetricsContext = useApiQueryContext(fetchPlayerMetrics, onPlayerMetrics, filterString);

    React.useEffect(() => {
        if (player && !player.metrics) {
            playerMetricsContext.enqueue([player.player.username]);
        }
    }, [player, playerMetricsContext]);

    return (
        <div className='player-overview-cont'>
            <div className='base-overview-row'>
                {renderOverviewItem('Total Wins', player.player.numWins)}
                {renderOverviewItem('Rank', player.player.rank)}
                {renderOverviewItem('Peak Rank', player.rankPeaks[0].rank, formatRankPeakDates(player.rankPeaks, dateRange[1]))}
            </div>
            <div className='detail-view'>
                <div className='detail-col'>
                    {renderOverviewItem(
                        'Average Solve Time',
                        formatDuration(player.metrics?.avgSolveTime))}
                    {renderOverviewItem(
                        'Min Solve Time',
                        formatDuration(player.metrics?.minSolveTime))}
                    {renderOverviewItem(
                        'Max Solve Time',
                        formatDuration(player.metrics?.maxSolveTime))}
                </div>
                <div className='detail-col'>
                    {renderOverviewItem(
                        'Average Post Delay',
                        formatDuration(player.metrics?.avgPostDelay))}
                    {renderOverviewItem(
                        'Min Post Delay',
                        formatDuration(player.metrics?.minPostDelay))}
                    {renderOverviewItem(
                        'Max Post Delay',
                        formatDuration(player.metrics?.maxPostDelay))}
                </div>
                <div className='detail-col'>
                    {renderOverviewItem(
                        'Average Round Length',
                        formatDuration(player.metrics?.avgRoundLength))}
                    {renderOverviewItem(
                        'Min Round Length',
                        formatDuration(player.metrics?.minRoundLength))}
                    {renderOverviewItem(
                        'Max Round Length',
                        formatDuration(player.metrics?.maxRoundLength))}
                </div>
            </div>
            <div className='view-rounds-link'>
                <Link path='/rounds' query={{ filter: `hostName eq "${player.player.username}"` }}>
                    {linkProps => <a {...linkProps}>Explore rounds by {player.player.username}</a>}
                </Link>
            </div>
        </div>
    );
}

function renderOverviewItem(key: string, value: any, title?: string) {
    return (
        <div className='overview-item' title={title}>
            <span className='key'>{key}</span><span className='value'>{value}</span>
        </div>
    );
}
