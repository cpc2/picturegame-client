import * as React from 'react';

import { useQueryString, QueryParam } from '../../hooks/History';
import { useDocumentTitle } from '../../hooks/Misc';
import { RoundFieldTranslations } from '../../model/Translation';
import { getInt } from '../../utils/QueryUtils';

import { Button } from '../base/Button';
import { Filter } from '../filter/Filter';
import { RoundFieldSpec } from '../filter/FilterSpecs';
import { GetSortSpec } from '../filter/FilterUtils';
import { FieldSelector } from '../misc/FieldSelector';

import { RoundList } from './RoundList';

import './RoundsPage.scss';

export default function RoundsPage() {
    const [queryFilter, setQueryFilter] = useQueryString(QueryParam.Filter);
    const [rawPage, setPage] = useQueryString(QueryParam.Page);

    const [querySort, setQuerySort] = useQueryString(QueryParam.Sort);
    const [sortField, sortDir] = GetSortSpec(querySort, RoundFieldSpec, 'roundNumber');
    function onSetSortField(field: string) {
        setQuerySort(`${field} ${sortDir}`);
    }
    function onToggleSortDir() {
        setQuerySort(`${sortField} ${sortDir === 'asc' ? 'desc' : 'asc'}`);
    }

    const [filterInvalid, setFilterInvalid] = React.useState(false);

    const page = getInt(rawPage, 0);

    useDocumentTitle(() => 'Explore Rounds', []);

    return (
        <div className='page-viewport'>
            <div className='page-body'>
                <Filter
                    className='rounds-page-filter'
                    fieldTranslations={RoundFieldTranslations}
                    filterSpec={RoundFieldSpec}
                    filterString={queryFilter ?? ''}
                    setFilterString={setQueryFilter}
                    invalid={filterInvalid}
                />
                <div className='sort-selector-cont'>
                    <label>Sort:</label>
                    <FieldSelector
                        fieldTranslations={RoundFieldTranslations}
                        value={sortField}
                        onChange={onSetSortField}
                    />
                    <Button onClick={onToggleSortDir}>
                        <i className={'fas fa-arrow-' + (sortDir === 'asc' ? 'up' : 'down')} />
                    </Button>
                </div>
                <RoundList
                    filter={queryFilter ?? ''}
                    sort={`${sortField} ${sortDir}`}
                    header={numResults => `${numResults} Result${numResults !== 1 ? 's' : ''}`}
                    filterInvalid={filterInvalid}
                    onRequestComplete={success => setFilterInvalid(!success)}
                    page={page}
                    onSetPage={setPage}
                    roundsPerPage={20}
                />
            </div>
        </div>
    );
}
