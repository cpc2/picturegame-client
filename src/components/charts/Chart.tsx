import * as _ from 'lodash-es';
import * as React from 'react';

import { TooltipPayload, TooltipProps } from 'recharts';

import { useCancellableAsync } from '../../hooks/Promise';
import { combineProps, ClassStyleProps } from '../../utils/CssUtils';

import { Button } from '../base/Button';
import { LoadingState } from '../base/LoadingState';

import './Chart.scss';

function useRecharts() {
    const [Recharts, setRecharts] = React.useState<typeof import('recharts') | null>(null);
    useCancellableAsync(() => import(/* webpackChunkName: "recharts" */'recharts'), setRecharts);
    return Recharts;
}

export interface ChartRenderer {
    (Recharts: typeof import('recharts')): React.ReactChild;
}

export interface ChartWrapperProps extends ClassStyleProps {
    loaded: boolean;
    header: string;
    renderChart: ChartRenderer;

    isZoomed?: boolean;
    resetZoom?: () => void;
    dragToZoomActive?: boolean;
    onToggleDragToZoom?: () => void;
    zoomable?: boolean; // default true
    containerRef?: React.RefObject<HTMLDivElement>;

    actionButtons?: React.ReactNode;
}

export function renderXLabel(Label: typeof import('recharts').Label, labelText: string) {
    return <Label value={labelText} position='insideBottom' offset={0} />;
}

export function renderYLabel(Label: typeof import('recharts').Label, labelText: string, position: 'right' | 'left' = 'left') {
    return <Label
        value={labelText}
        angle={-90}
        position={position === 'left' ? 'insideLeft' : 'insideRight'}
        offset={0}
        style={{ textAnchor: 'middle' }}
    />;
}

export function ChartWrapper(props: ChartWrapperProps) {
    const Recharts = useRecharts();

    let body: React.ReactChild;
    if (!Recharts || !props.loaded) {
        body = <LoadingState />;
    } else {
        body = (<React.Fragment>
            <header>
                <div className='chart-header-buttons float-left'>
                    {props.actionButtons}
                </div>
                <div className='chart-header-text'>{props.header}</div>
                <div className='chart-header-buttons float-right'>
                    {props.zoomable !== false && props.isZoomed && <Button title='Reset zoom' onClick={props.resetZoom}>
                        <i className='fas fa-search-minus' />
                    </Button>}
                    {props.zoomable !== false && <Button
                        active={props.dragToZoomActive}
                        title='Zoom (drag on the chart to select a domain)'
                        onClick={props.onToggleDragToZoom}
                    >
                        <i className='fas fa-search' />
                    </Button>}
                </div>
            </header>
            <div className='chart-container' ref={props.containerRef}>
                <Recharts.ResponsiveContainer>
                    {props.renderChart(Recharts)}
                </Recharts.ResponsiveContainer>
            </div>
        </React.Fragment>);
    }

    return (<div {...combineProps(props, 'chart-card')}>
        {body}
    </div>);
}

interface ExtendedTooltipProps extends TooltipProps {
    getLabel?: (payload?: ReadonlyArray<TooltipPayload>) => React.ReactNode;
    nameFormatter?: (name: string) => string;
}

export function ChartTooltip(props: ExtendedTooltipProps) {
    let label: React.ReactNode | null = props.label ?? (props.getLabel?.(props.payload));
    label = (props.labelFormatter && (typeof label === 'string' || typeof label === 'number')) ? props.labelFormatter(label) : label;

    return (
        <div className='chart-tooltip-cont'>
            {!_.isNil(label) && <div className='chart-tooltip-header'>{label}</div>}
            <div className='chart-tooltip-body'>
                {props.payload?.map((v, i) => [
                    <div
                        key={v.name + '_name'}
                        className='chart-tooltip-cell chart-tooltip-username'
                        style={{ color: v.color ?? v.payload?.fill }}
                    >
                        {props.nameFormatter ? props.nameFormatter(v.name) : v.name}
                    </div>,
                    <div
                        key={v.name + '_value'}
                        className='chart-tooltip-cell chart-tooltip-count'
                        style={{ color: v.color ?? v.payload?.fill }}
                    >
                        {props.formatter ? props.formatter(v.value as string | number, v.name, v, i) : v.value}
                    </div>,
                ])}
            </div>
        </div>
    );
}
