import * as API from 'picturegame-api-wrapper';
import * as React from 'react';
import * as Reselect from 'reselect';

import { useDateRange } from '../../hooks/Misc';
import { useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import { calculateLinearDateTicks, getYRange, useChartZoom, ZoomableChartProps } from '../../utils/ChartUtils';
import { getColour } from '../../utils/CssUtils';
import { DateRange } from '../../utils/DateUtils';
import { formatDate } from '../../utils/Formatter';
import { getRankChanges } from '../../utils/LeaderboardUtils';
import { createMemoizedArray } from '../../utils/Memoize';
import { roundToPowerTen } from '../../utils/NumUtils';

import { ChartTooltip, ChartWrapper } from './Chart';

interface RankData {
    time: number;
    [username: string]: number;
}

interface Props extends ZoomableChartProps {
    activePlayers: API.LeaderboardEntry[];
}

function stateToProps(state: UI.State, activePlayers: API.LeaderboardEntry[], dateRange: DateRange) {
    return {
        ranks: getRankList(state, activePlayers, dateRange),
        colours: state.userConfig.playerColours,
    };
}

const getRankList: (state: UI.State, activePlayers: API.LeaderboardEntry[], dateRange: DateRange) => RankData[]
    = Reselect.createSelector(
        getRankChanges,
        createMemoizedArray((_: UI.State, activePlayers: API.LeaderboardEntry[]) => activePlayers),
        (_s: UI.State, _p: API.LeaderboardEntry[], dateRange: DateRange) => dateRange,
        function _getRankList(changes, activeUsers, dateRange): RankData[] {
            if (!changes.length || !activeUsers?.length) {
                return [];
            }

            const data: RankData[] = [];

            const currentRanks: { [name: string]: number } = {};

            function addDataPoint(time: number) {
                data.push({ time, ...currentRanks });
            }

            for (const { from, to, time, end } of changes) {
                let changed = false;

                if (!from) {
                    // New player
                    for (const player of activeUsers) {
                        if (end === player.rank) {
                            currentRanks[player.username] = to;
                            changed = true;
                            break;
                        }
                    }
                } else {
                    // Existing player ranked up
                    for (const username in currentRanks) {
                        const currentRank = currentRanks[username];
                        if (currentRank === from) {
                            currentRanks[username] = to;
                            changed = true;
                        } else if (from > currentRank && to <= currentRank) {
                            ++currentRanks[username];
                            changed = true;
                        }
                    }
                }

                if (changed) {
                    addDataPoint(time);
                }
            }

            const [, rangeEnd] = dateRange;
            const chartEnd = Math.floor((rangeEnd?.valueOf() ?? Date.now()) / 1000);
            addDataPoint(chartEnd);

            return data;
        });

export function RankChart(props: Props) {
    const dateRange = useDateRange();
    const { ranks, colours } = useState(stateToProps, props.activePlayers, dateRange);

    const zoomProps = useChartZoom({
        data: ranks,
        xKey: 'time',
        activeDomain: props.zoomToDomain,
        setActiveDomain: props.onZoom,
        dragToZoomActive: props.dragToZoomActive,
    });

    const [yMin, yMax] = getYRange(ranks, zoomProps.xMin, zoomProps.xMax, 'time', datum => props.activePlayers.map(p => datum[p.username]));

    return <ChartWrapper
        header='Rank over time'
        loaded={!!ranks.length}
        isZoomed={props.zoomToDomain !== null}
        resetZoom={() => props.onZoom(null)}
        dragToZoomActive={props.dragToZoomActive}
        onToggleDragToZoom={props.onToggleDragToZoom}
        containerRef={zoomProps.containerRef}
        renderChart={({ LineChart, XAxis, Label, YAxis, Line, Tooltip, ReferenceArea }) => {
            return <LineChart
                data={ranks}
                margin={{ top: 20, left: 20, bottom: 20, right: 20 }}
                onMouseDown={zoomProps.onMouseDown}
                onMouseMove={zoomProps.onMouseMove}
            >
                <XAxis
                    dataKey='time'
                    type='number'
                    domain={[zoomProps.xMin, zoomProps.xMax]}
                    tickFormatter={formatDate}
                    ticks={calculateLinearDateTicks(zoomProps.xMin, zoomProps.xMax)}
                    allowDataOverflow={true}
                >
                    <Label value='Date' position='bottom' offset={0} />
                </XAxis>
                <YAxis
                    reversed={true}
                    domain={[roundToPowerTen(yMin, Math.floor), roundToPowerTen(yMax, Math.ceil)]}
                    scale='log'
                    allowDataOverflow={true}
                >
                    <Label
                        value='Rank'
                        angle={-90}
                        position='left'
                        offset={0}
                        style={{ textAnchor: 'middle' }} />
                </YAxis>
                {props.activePlayers.map((p, i) => <Line
                    key={p.username}
                    type='stepAfter'
                    dataKey={p.username}
                    stroke={colours[p.username] ?? getColour(i)}
                    dot={false}
                    isAnimationActive={false}
                />)}
                <Tooltip labelFormatter={formatDate} content={<ChartTooltip />} />
                {zoomProps.dragStart && zoomProps.dragEnd ? (
                    <ReferenceArea x1={zoomProps.dragStart} x2={zoomProps.dragEnd} />
                ) : null}
            </LineChart>;
        }}
    />;
}
