import { minBy } from 'lodash-es';
import * as API from 'picturegame-api-wrapper';
import * as React from 'react';
import * as Reselect from 'reselect';
import * as tinycolor from 'tinycolor2';

import { TooltipProps } from 'recharts';

import { SetHideWeeklyWinsMovingAverage } from '../../actions/UserConfigActions';

import { useDateRange } from '../../hooks/Misc';
import { useDispatch, useState } from '../../hooks/Redux';

import * as UI from '../../model/ui';

import { calculateLinearDateTicks, calculateLinearTicks, getYRange, useChartZoom, ZoomableChartProps } from '../../utils/ChartUtils';
import { nextOccurence } from '../../utils/CollectionUtils';
import { getColour } from '../../utils/CssUtils';
import { addDays, moveToStartOfWeek, DateRange } from '../../utils/DateUtils';
import { formatDate } from '../../utils/Formatter';
import { createMemoizedArray } from '../../utils/Memoize';

import { ChartWrapper } from './Chart';

import { Button } from '../base/Button';

interface Props extends ZoomableChartProps {
    activePlayers: API.LeaderboardEntry[];
}

interface WeekyWins {
    date: number;
    [key: string]: number;
}

function stateToProps(state: UI.State) {
    return {
        colours: state.userConfig.playerColours,
        theme: state.userConfig.theme ?? 'dark',
        showMovingAverage: !state.userConfig.weeklyWinsHideMovingAverage,
    };
}

function dispatchToProps(dispatch: UI.DispatchAction) {
    return {
        toggleMovingAverage: (value: boolean) => dispatch(SetHideWeeklyWinsMovingAverage(value)),
    };
}

const MovingAverageNumWeeks = 5;

const getWeeklyWinCounts: (activePlayers: API.LeaderboardEntry[], dateRange: DateRange) => WeekyWins[] = Reselect.createSelector(
    createMemoizedArray((activePlayers: API.LeaderboardEntry[]) => activePlayers),
    (_: API.LeaderboardEntry[], dateRange: DateRange) => dateRange,
    function _getWeeklyWinCounts(activeUsers, dateRange): WeekyWins[] {
        if (!activeUsers) { return []; }
        const data: WeekyWins[] = [];

        const cursor = new Map<string, number>(activeUsers.map(u => [u.username, 0]));

        const chartEnd = dateRange[1] ?? new Date();
        let weekStart = dateRange[0] ? new Date(dateRange[0]) :
            new Date(minBy(activeUsers, u => u.winTimeList![0])!.winTimeList![0] * 1000);
        moveToStartOfWeek(weekStart);

        function movingAverage(username: string, nextCount: number) {
            let total = nextCount;
            for (let offset = 1; offset < MovingAverageNumWeeks; ++offset) {
                if (data.length < offset) {
                    break;
                }
                total += data[data.length - offset][username];
            }
            return total / MovingAverageNumWeeks;
        }

        while (weekStart < chartEnd) {
            const weekEnd = addDays(new Date(weekStart), 7);
            const weekEndTimestamp = weekEnd.valueOf() / 1000;

            const thisWeekWins: WeekyWins = { date: weekStart.valueOf() / 1000 };

            for (const player of activeUsers) {
                const startIndex = cursor.get(player.username)!;
                const endIndex = nextOccurence(player.winTimeList!, time => time > weekEndTimestamp, startIndex);
                const playerWins = endIndex - startIndex;
                thisWeekWins[player.username] = playerWins;
                thisWeekWins[`${player.username}/movingAverage`] = movingAverage(player.username, playerWins);

                if (playerWins) {
                    cursor.set(player.username, endIndex);
                }
            }

            data.push(thisWeekWins);

            weekStart = weekEnd;
        }

        return data;
    });

export function WeeklyWinsChart(props: Props) {
    const { colours, theme, showMovingAverage } = useState(stateToProps);
    const { toggleMovingAverage } = useDispatch(dispatchToProps);
    const dateRange = useDateRange();
    const weeklyWins = getWeeklyWinCounts(props.activePlayers, dateRange);

    const zoomProps = useChartZoom({
        data: weeklyWins,
        xKey: 'date',
        activeDomain: props.zoomToDomain,
        setActiveDomain: props.onZoom,
        dragToZoomActive: props.dragToZoomActive,
    });

    const [, yMax] = getYRange(weeklyWins, zoomProps.xMin, zoomProps.xMax, 'date',
        datum => props.activePlayers.map(p => datum[p.username]));
    const yTicks = calculateLinearTicks(0, yMax);

    function lineColour(colour: string) {
        const colourObj = tinycolor.fromRatio(colour);
        return theme === 'dark'
            ? colourObj.lighten(25).toRgbString()
            : colourObj.darken(10).toRgbString();
    }

    function CustomTooltip(ttProps: TooltipProps) {
        return (
            <div className='chart-tooltip-cont'>
                <div className='chart-tooltip-header'>{formatDate(ttProps.label)}</div>
                <div className='chart-tooltip-body'>
                    {ttProps.payload?.filter(v => !v.name.endsWith('/movingAverage')).map(v => [
                        <div key={v.name + '_name'} className='chart-tooltip-cell chart-tooltip-username' style={{ color: v.color }}>
                            {v.name}
                        </div>,
                        <div key={v.name + '_value'} className='chart-tooltip-cell chart-tooltip-count' style={{ color: v.color }}>
                            {v.value} {showMovingAverage && `(avg. ${v.payload[v.name + '/movingAverage']})`}
                        </div>,
                    ])}
                </div>
            </div>
        );
    }

    return <ChartWrapper
        header='Weekly wins'
        loaded={true}
        isZoomed={props.zoomToDomain !== null}
        resetZoom={() => props.onZoom(null)}
        dragToZoomActive={props.dragToZoomActive}
        onToggleDragToZoom={props.onToggleDragToZoom}
        containerRef={zoomProps.containerRef}
        actionButtons={
            <Button
                title={`Toggle ${MovingAverageNumWeeks}-week moving average line`}
                onClick={() => toggleMovingAverage(showMovingAverage)}
                active={showMovingAverage}
            >
                <i className='fas fa-chart-line' />
            </Button>
        }
        renderChart={({ ComposedChart, Bar, XAxis, YAxis, Label, Line, Tooltip, ReferenceArea }) => {
            return (<ComposedChart
                data={weeklyWins}
                margin={{ top: 21, left: 20, bottom: 20, right: 20 }}
                onMouseDown={zoomProps.onMouseDown}
                onMouseMove={zoomProps.onMouseMove}
            >
                <XAxis
                    dataKey='date'
                    type='number'
                    domain={[zoomProps.xMin, zoomProps.xMax]}
                    ticks={calculateLinearDateTicks(zoomProps.xMin, zoomProps.xMax)}
                    tickFormatter={formatDate}
                    allowDataOverflow={true}
                >
                    <Label value='Date' position='bottom' offset={0} />
                </XAxis>
                <YAxis
                    ticks={yTicks}
                    allowDataOverflow={true}
                    domain={[0, yTicks[yTicks.length - 1]]}
                >
                    <Label
                        value='Number of Wins'
                        angle={-90}
                        position='left'
                        offset={0}
                        style={{ textAnchor: 'middle' }} />
                </YAxis>
                {props.activePlayers.map((p, i) =>
                    <Bar
                        key={p.username + '/count'}
                        dataKey={p.username}
                        fill={colours[p.username] ?? getColour(i)}
                        isAnimationActive={false}
                    />)}
                {showMovingAverage && props.activePlayers.map((p, i) =>
                    <Line
                        key={p.username + '/movingAverage'}
                        dataKey={p.username + '/movingAverage'}
                        stroke={lineColour(colours[p.username] ?? getColour(i))}
                        isAnimationActive={false}
                        dot={false}
                        strokeWidth={props.activePlayers.length === 1 ? 2 : 1}
                    />)}
                <Tooltip labelFormatter={formatDate} content={<CustomTooltip />} />
                {zoomProps.dragStart && zoomProps.dragEnd ? (
                    <ReferenceArea x1={zoomProps.dragStart} x2={zoomProps.dragEnd} />
                ) : null}
            </ComposedChart>);
        }}
    />;
}
