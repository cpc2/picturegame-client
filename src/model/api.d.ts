import { Round } from 'picturegame-api-wrapper';

export interface RoundQuery {
    filter?: string;
    select?: ReadonlyArray<keyof Round>;
    sort?: string | string[];
    offset?: number;
    limit?: number;
}
