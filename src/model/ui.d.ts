import * as Immutable from 'immutable';
import * as API from 'picturegame-api-wrapper';

export interface State {
    config: Config;
    userConfig: UserConfig;
    data: DataState;
    leaderboard: LeaderboardState;
    view: ViewState;
}

export interface Config {
    apiUrl: string;
    pageSize: number;
    subreddit: string;
    discordInvite: string;
    version: string;
}

export type ThemeSetting = 'light' | 'dark';
export type ChartId = 'rounds' | 'ranks' | 'weekly_wins';

export interface UserConfig {
    theme?: ThemeSetting | null;
    playerColours: Record<string, string>;
    weeklyWinsHideMovingAverage: boolean;
    chartVisibility: Record<ChartId, boolean>;
}

export interface DataState {
    leaderboard: Immutable.Map<string, API.LeaderboardEntry>;
    playerMetrics: Immutable.Map<string, API.PlayerMetrics>;
}

export interface Player {
    player: API.LeaderboardEntry;
    metrics?: API.PlayerMetrics;
    rankPeaks: RankIntervalInfo[];
}

export interface RankIntervalInfo {
    rank: number;
    fromTime: number;
    toTime?: number;
}

export type PlayerHostAggs = Pick<API.Round, 'hostName'> & Pick<API.RoundAggregates,
    'avgPostDelay' | 'minPostDelay' | 'maxPostDelay' |
    'avgSolveTime' | 'minSolveTime' | 'maxSolveTime' |
    'numRounds'>;

export type PlayerWinnerAggs = Pick<API.Round, 'winnerName'> & Pick<API.RoundAggregates,
    'avgSolveTime' | 'minSolveTime' | 'maxSolveTime' |
    'minWinTime' | 'maxWinTime'>;

export interface LeaderboardState {
    jumpedPlayer?: string;
    itemsPerPage: number;
    dataLoaded: boolean;
}

export interface ViewState {
    popups: PopupMessage[];
}

export interface PopupMessage {
    id: string;
    message: string;
    timeout?: number;
    dismissable?: boolean;
    type: 'info' | 'warn' | 'error';
}

export interface BaseAction {
    type: string;
}

export interface Action<Args> extends BaseAction {
    args: Args;
}

export interface ActionCreator<A> {
    (args: A): Action<A>;
    type: string;
}

export interface NoPayloadActionCreator {
    (): BaseAction;
    type: string;
}

export interface PersistedActionCreator<A extends string> {
    (args: A): ThunkAction;
    action: ActionCreator<A | null>;
}

export interface Reducer<S, A> {
    (state: S, args: A): S;
}

export interface DispatchAction {
    (action: BaseAction): void;
    <TArgs>(action: Action<TArgs>): void;
    <TResult>(thunk: ThunkAction<TResult>): TResult;
}

export interface ThunkAction<TResult = void> {
    (dispatch: DispatchAction, getState: () => State): TResult;
}
