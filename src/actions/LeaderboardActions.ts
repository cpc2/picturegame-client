import { createActionCreator } from '../state/Actions';

export const SetJumpedPlayer = createActionCreator<string>('Leaderboard.SetJumpedPlayer');
