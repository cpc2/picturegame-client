import * as API from 'picturegame-api-wrapper';

import { createActionCreator } from '../state/Actions';

export interface UpdateLeaderboardArgs {
    players: API.LeaderboardEntry[];
}
export const UpdateLeaderboard = createActionCreator<UpdateLeaderboardArgs>('Data.UpdateLeaderboard');

export const ClearPlayerData = createActionCreator<undefined>('Data.ClearPlayerData');

export interface UpdatePlayerMetricsArgs {
    players: API.Player[];
    names: string[];
}
export const UpdatePlayerMetrics = createActionCreator<UpdatePlayerMetricsArgs>('Data.UpdatePlayerMetrics');
