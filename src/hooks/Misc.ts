import * as React from 'react';

import { useQueryString, QueryParam } from './History';

import { getDateRange, DateRange } from '../utils/DateUtils';

export function useReplicatedState<T>(defaultValue: T) {
    const [value, setValue] = React.useState(defaultValue);
    const refValue = React.useRef(defaultValue);

    React.useEffect(() => {
        refValue.current = value;
    }, [value]);

    return { value, setValue, refValue };
}

export function useSyncedState<T>(valueFunc: () => T, dependencies: any[]): [T, React.Dispatch<React.SetStateAction<T>>] {
    const [value, setValue] = React.useState(valueFunc);
    React.useEffect(() => {
        setValue(valueFunc());
    }, dependencies);
    return [value, setValue];
}

export function useDocumentTitle(titleFunc: () => string, dependencies: any[]) {
    React.useLayoutEffect(() => {
        const components = document.title.split('|').map(c => c.trim());
        const newTitle = [titleFunc(), components[components.length - 1]].join(' | ');
        document.title = newTitle;
    }, dependencies);
}

export function useDateRange(): DateRange {
    const [rangeType] = useQueryString(QueryParam.DateFilter);
    const [customStart] = useQueryString(QueryParam.CustomStartDate);
    const [customEnd] = useQueryString(QueryParam.CustomEndDate);
    return getDateRange({ rangeType, customStart, customEnd });
}
