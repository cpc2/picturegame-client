import * as React from 'react';

export function useEvent<K extends keyof WindowEventMap>(
    type: K,
    listener: (this: Window, ev: WindowEventMap[K]) => any,
    options?: boolean | AddEventListenerOptions) {

    React.useEffect(() => {
        window.addEventListener(type, listener, options);

        return () => {
            window.removeEventListener(type, listener);
        };
    }, []);
}
