import * as React from 'react';

export function useToggle(intialValue: boolean) {
    const [value, setValue] = React.useState(intialValue);
    const setTrue = React.useCallback(() => setValue(true), []);
    const setFalse = React.useCallback(() => setValue(false), []);

    return {
        value,
        setTrue,
        setFalse,
    };
}
