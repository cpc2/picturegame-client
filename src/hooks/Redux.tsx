import * as React from 'react';
import * as Redux from 'redux';

import * as UI from '../model/ui';

import { arrayShallowEqual, shallowEqual } from '../utils/Equality';

import { useForceUpdate } from './ForceUpdate';

export const StoreContext = React.createContext<Redux.Store<UI.State, Redux.AnyAction>>(null as any);

export function useState<TProps, TArgs extends any[]>(
    stateToProps: (state: UI.State, ...args: TArgs) => TProps,
    ...args: TArgs) {

    const store = React.useContext(StoreContext);
    const forceUpdate = useForceUpdate();

    const outputRef = React.useRef<TProps>(null as any);
    const argsRef = React.useRef<TArgs>(null as any);

    // Always run on first render (argsRef.current = null !== args)
    if (!arrayShallowEqual(argsRef.current, args)) {
        argsRef.current = args;
        outputRef.current = stateToProps(store.getState(), ...args);
    }

    function updatePropsIfChanged() {
        const nextProps = stateToProps(store.getState(), ...argsRef.current);
        if (!shallowEqual(outputRef.current, nextProps)) {
            outputRef.current = nextProps;
            forceUpdate();
        }
    }

    React.useEffect(() => store.subscribe(updatePropsIfChanged), [store]);

    return outputRef.current;
}


export function useDispatch<TProps, TArgs extends any[]>(
    dispatchToProps: (dispatch: UI.DispatchAction, ...args: TArgs) => TProps,
    ...args: TArgs) {

    const store = React.useContext(StoreContext);
    return React.useMemo(() => dispatchToProps(store.dispatch, ...args), [store.dispatch, ...args]);
}
