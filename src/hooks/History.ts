import * as History from 'history';
import * as QueryString from 'query-string';
import * as React from 'react';

export const HistoryContext = React.createContext<History.History>(null as any);

type QueryString = string | undefined | null;
type QueryStringArray = string[] | undefined | null;

export enum QueryParam {
    DateFilter = 'dateFilter',
    Player = 'player',
    Players = 'players',
    Page = 'page',
    Filter = 'filter',
    Sort = 'sort',
    CustomStartDate = 'customStartDate',
    CustomEndDate = 'customEndDate',
}

export const GlobalQueryParams: ReadonlySet<QueryParam> = new Set([
    QueryParam.DateFilter,
    QueryParam.CustomStartDate,
    QueryParam.CustomEndDate,
]);

export function useQueryString(key: QueryParam): [QueryString, (value: any) => void] {
    const [value, setValue] = useQuery(key);
    return [
        Array.isArray(value) ? value[0] : value,
        setValue,
    ];
}

export function useQueryArray(key: QueryParam): [QueryStringArray, (value: any[]) => void] {
    const [value, setValue] = useQuery(key);
    return [
        typeof value === 'string' ? [value] : value,
        setValue,
    ];
}

function useQuery(key: string): [QueryString | QueryStringArray, (value: any) => void] {
    const historyContext = React.useContext(HistoryContext);

    const query = QueryString.parse(historyContext.location.search);
    const queryVal = query[key];

    const [value, setValue] = React.useState(queryVal);

    function setFromQuery() {
        const newQuery = QueryString.parse(historyContext.location.search);
        setValue(newQuery[key]);
    }

    // Empty array forces React to only run this at mount and unmount
    React.useEffect(() => historyContext.listen(setFromQuery), []);

    const setToQuery = React.useCallback((newValue: any) => {
        const newSearch = '?' + QueryString.stringify({
            ...QueryString.parse(historyContext.location.search),
            [key]: newValue,
        });
        if (newSearch !== historyContext.location.search) {
            historyContext.push({ search: newSearch });
        }
    }, [key]);

    return [
        value,
        setToQuery,
    ];
}

export function usePath() {
    const historyContext = React.useContext(HistoryContext);

    const [path, setPath] = React.useState(historyContext.location.pathname);

    function setFromPath() {
        setPath(historyContext.location.pathname);
    }

    React.useEffect(() => historyContext.listen(setFromPath), []);

    return path;
}
