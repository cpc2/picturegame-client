import { PicturegameApi } from './PicturegameApi';

export function fetchPlayerMetrics(usernames: string[], api: PicturegameApi, filter?: string) {
    return api.getPlayerMetrics(usernames, filter);
}
