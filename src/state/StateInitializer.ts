import * as Redux from 'redux';

import { ReplaceUserConfig } from '../actions/UserConfigActions';

import * as UI from '../model/ui';

import { ReducerFactory } from '../reducers';

import { thunk } from './Middleware';
import { SaveStateKey, SaveStateMiddleware } from './Storage';
import { Store } from './Store';

export async function getStore() {
    const reducer = await ReducerFactory.createReducer();

    if (!Store.store) {
        Store.store = createStore(reducer);
        Store.reducer = reducer;
    } else {
        console.log('Hot Reloading');
    }

    if (reducer !== Store.reducer) {
        console.log('Replacing reducer');
        Store.store.replaceReducer(reducer);
        Store.reducer = reducer;
    }

    return Store.store;
}

function createStore(reducer: Redux.Reducer<UI.State>) {
    console.log('Creating Store');
    const compose = (window as any)['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] ?? Redux.compose;

    const saveStateMiddleware = createSaveSaveStateMiddleware();
    const store = Redux.createStore(reducer, compose(Redux.applyMiddleware(
        thunk,
        saveStateMiddleware.middleware)));

    saveStateMiddleware.subscribe(store.dispatch);
    return store;
}

function createSaveSaveStateMiddleware() {
    const middleware = new SaveStateMiddleware();

    middleware.registerSaveState(SaveStateKey.UserConfig, { extract: s => s.userConfig, unpack: ReplaceUserConfig });

    return middleware;
}
