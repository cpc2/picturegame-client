import * as API from 'picturegame-api-wrapper';

import * as UI from '../model/ui';

import { SaveStateKey } from './Storage';

export const initialConfigState: UI.Config = {
    apiUrl: 'http://localhost:9001',
    pageSize: 50,
    subreddit: 'PGProvium',
    discordInvite: 'RCEFpXk',
    version: '0',
};

export function getInitialUserConfigState(): UI.UserConfig {
    return withLocalStorage<UI.UserConfig>(SaveStateKey.UserConfig, {
        playerColours: {},
        weeklyWinsHideMovingAverage: false,
        chartVisibility: {
            ranks: true,
            rounds: true,
            weekly_wins: true,
        },
    });
}

export async function getInitialDataState(): Promise<UI.DataState> {
    const Immutable = await import(/* webpackChunkName: "immutable" */'immutable');
    return {
        leaderboard: Immutable.Map<string, API.LeaderboardEntry>(),
        playerMetrics: Immutable.Map<string, API.PlayerMetrics>(),
    };
}

export const initialLeaderboardState: UI.LeaderboardState = {
    itemsPerPage: 50,
    dataLoaded: false,
};

export const initialViewState: UI.ViewState = {
    popups: [],
};

export async function getInitialState(): Promise<UI.State> {
    return {
        config: initialConfigState,
        userConfig: getInitialUserConfigState(),
        data: await getInitialDataState(),
        leaderboard: initialLeaderboardState,
        view: initialViewState,
    };
}

function withLocalStorage<T>(key: SaveStateKey, defaultState: T): T {
    try {
        const raw = window.localStorage.getItem(key);
        const parsed = raw && JSON.parse(raw);
        return Object.assign({}, defaultState, parsed);
    } catch (e) {
        console.error(`Failed to load state from localStorage for key ${key}`, e);
        return defaultState;
    }
}
