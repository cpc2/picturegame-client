import * as Redux from 'redux';

import * as UI from '../model/ui';

export interface IStore {
    store?: Redux.Store<UI.State>;
    reducer?: Redux.Reducer<UI.State>;
}

export const Store: IStore = {};
