import * as Redux from 'redux';

import * as UI from '../model/ui';

import * as Initial from '../state/InitialState';

import * as Config from './ConfigReducer';
import * as Data from './DataReducer';
import * as Leaderboard from './LeaderboardReducer';
import * as UserConfig from './UserConfigReducer';
import * as View from './ViewReducer';

export class ReducerFactory {
    private static reducer: Redux.Reducer<UI.State>;

    public static async createReducer(): Promise<Redux.Reducer<UI.State>> {
        if (!ReducerFactory.reducer) {
            const configReducer = Config.init(Initial.initialConfigState);
            const userConfigReducer = UserConfig.init(Initial.getInitialUserConfigState());
            const dataReducer = Data.init(await Initial.getInitialDataState());
            const leaderboardReducer = Leaderboard.init(Initial.initialLeaderboardState);
            const viewReducer = View.init(Initial.initialViewState);

            ReducerFactory.reducer = Redux.combineReducers<UI.State>({
                config: configReducer.getReducer(),
                userConfig: userConfigReducer.getReducer(),
                data: dataReducer.getReducer(),
                leaderboard: leaderboardReducer.getReducer(),
                view: viewReducer.getReducer(),
            });
        }

        return ReducerFactory.reducer;
    }
}

