import * as History from 'history';
import * as React from 'react';

import { SetConfig } from './actions/ConfigActions';
import { Root } from './components/Root';

import { ApiContext, PicturegameApi } from './api/PicturegameApi';

import { HistoryContext } from './hooks/History';
import { StoreContext } from './hooks/Redux';

import * as UI from './model/ui';

import { getStore } from './state/StateInitializer';

export async function load() {
    const store = await getStore();
    const history = History.createBrowserHistory();

    const configPromise = fetch('config.json').then(res => res.json());
    const ReactDOMPromise = import(/* webpackChunkName: "react-dom" */'react-dom');

    const config: UI.Config = await configPromise;
    store.dispatch(SetConfig(config));

    const api = new PicturegameApi(config.apiUrl, store.dispatch);

    const ReactDOM = await ReactDOMPromise;

    const loadingDiv = document.getElementById('fullpage-loading');
    loadingDiv?.remove();

    ReactDOM.render(
        <HistoryContext.Provider value={history}>
            <StoreContext.Provider value={store}>
                <ApiContext.Provider value={api}>
                    <Root />
                </ApiContext.Provider>
            </StoreContext.Provider>
        </HistoryContext.Provider>,
        document.getElementById('root'));

    console.log('Main rendered');
}

if ((module as any).hot) {
    (module as any).hot.accept();
    if ((module as any).hot.status() === 'apply') {
        load();
    }
}
