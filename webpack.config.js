const path = require('path');
const webpack = require('webpack');

const CircularDependencyPlugin = require('circular-dependency-plugin');
const CheckerPlugin = require('fork-ts-checker-webpack-plugin');
const HtmlPlugin = require('html-webpack-plugin');
const ScriptExtHtmlPlugin = require('script-ext-html-webpack-plugin');

module.exports = {
    entry: {
        index: [
            'webpack-hot-middleware/client?noInfo=true',
            './src/client',
        ],
        vendor: [
            'react',
            'react-dom',
        ],
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx'],
    },
    devtool: 'cheap-module-eval-source-map',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                options: {
                    transpileOnly: true,
                },
            },
            {
                test: /\.scss$/,
                use: [
                    { loader: 'style-loader' },
                    { loader: 'css-loader' },
                    { loader: 'sass-loader' },
                ],
            },
            {
                test: /\.(gif|png|jpe?g|svg)$/i,
                use: ['file-loader', 'image-webpack-loader'],
            },
        ],
    },
    plugins: [
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new CircularDependencyPlugin({
            exclude: /node_modules/,
        }),
        new CheckerPlugin(),
        new HtmlPlugin({
            template: 'index-template.html',
            filename: 'index.html',
        }),
        new ScriptExtHtmlPlugin({
            custom: [
                {
                    test: /index.*\.js/,
                    attribute: 'onload',
                    value: 'window.PictureGame.load()',
                },
            ],
        }),
    ],
    output: {
        path: path.join(__dirname, 'app'),
        filename: '[name].js',
        publicPath: '/app/',
        library: 'PictureGame',
    },
    performance: {
        hints: false,
    },
    mode: 'development',
};
